=== Readers Hub ===
Contributors: SilverKenn
Tags: wordpress, plugin, metas, options
Requires at least: 4.0
Tested up to: 4.4.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin if for custom site functionality

== Installation ==

1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

For additional information, please contact kosherkenn12@gmail.com

== Changelog ==

= 1.0 =
* 2016-04-13
* Initial release

== Upgrade Notice ==

= 1.0 =
* 2016-04-13
* Initial release
