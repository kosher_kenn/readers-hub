<?php
/**
 * Package Included for Readers Hub
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  URL Rewrite Handles
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

// Duplication is not allowed
if( !class_exists( 'RH_Rewrite' ) ) {

    class RH_Rewrite {

        public $base    = '';
        public $url     = array();
        public $vars    = array();
        public $query   = array();
        public static $slug;

        public function __construct() {
            $this->init();
            add_action( 'init', array( $this, 'rewrite' ) );
            add_filter( 'query_vars', array( $this, 'query' ) );
            add_filter( 'template_include', array( $this, 'template') );
            add_action( 'rh/head', array( $this, 'redirect' ) );
            add_action( 'rh_page_title', array( $this, 'title' ) );
            add_filter( 'body_class',  array( $this, 'classes' ) );

            #add_action( 'rh/head', array( $this, '_check_rewrite' ) );
        }     

        public function _check_rewrite() {
            global $wp_rewrite;
            echo '<pre>', print_r( $wp_rewrite, 1), '</pre>';            
        }   

        public function init() {
            $this->base = apply_filters('rh/base/url','readers-hub');
            $this->url = array(
                $this->base                                 => 'index',
                $this->base .'/dashboard'                   => 'dashboard',
                $this->base .'/start'                       => 'start',
                $this->base .'/profile'                     => 'profile',
                $this->base .'/profile/edit'                => 'edit',
                $this->base .'/readings'                    => 'readings',
                $this->base .'/readings/in-progress'        => 'in-progress',
                $this->base .'/readings/awaiting-feedback'  => 'waiting-feedback',
                $this->base .'/readings/archived'           => 'archived',
                $this->base .'/readings/request'            => 'request',
                $this->base .'/availability'                => 'availability',
                $this->base .'/admin/members-list'          => 'members-list',
                $this->base .'/admin/reading-list'          => 'reading-list',
            );
            self::$slug = $this->url;
            return $this->url;
        }
        
        public static function rewrite() {
            foreach ( self::$slug as $slug => $file ) {
                add_rewrite_rule( "^{$slug}$", "index.php?{$slug}={$file}", "top" );
            }
            add_rewrite_rule( "^readers-hub/readings/?([0-9]{1,})/?$", 'index.php?type=readers_hub&rh=$matches[1]', "top");
            add_rewrite_rule( "^readers-hub/reader/?(.+)/?$", 'index.php?type=readers_hub&rh_username=$matches[1]', "top");

            add_rewrite_rule( 
                "^readers-hub/admin/reading-list/?([0-9]{1,})/?$", 
                'index.php?type=readers_hub&rr_list=$matches[1]', 
                "top"
            );

            add_rewrite_rule( 
                "^readers-hub/admin/members-list/?([0-9]{1,})/?$", 
                'index.php?type=readers_hub&mem_list=$matches[1]', 
                "top"
            );

        }

        public function query( $vars ) {
            foreach ( $this->init() as $slug => $file ) {
                $vars[] = $slug; 
            }
            $vars[] = 'type';
            $vars[] = 'rh';
            $vars[] = 'rh_username';
            $vars[] = 'mem_list';
            $vars[] = 'rr_list';
            return $vars;
        }

        public function template( $path ) {
            $template_dir = RH_Help::template_dir() ;
            foreach ( $this->init() as $slug => $file ) {
                if ( ( get_query_var( $slug ) && !is_admin() ) || get_query_var( 'type' ) === 'readers_hub' ) {
                    return ( file_exists( 
                        $template_dir.$file. '.php' ) 
                    ) ? $template_dir.$file. '.php' : $template_dir . 'index.php' ;
                } 
            }
            return $path;
        }

        public function classes( $classes ) {
            foreach ( $this->init() as $slug => $file ) {
                if ( get_query_var( $slug ) && !is_admin() ) {
                    $classes[] = 'page-'.$file;
                }
            }
            if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var('rh') ) {
                $classes[] = 'page-view-reading';
            } else if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var('rh_username') ) {
                $classes[] = 'page-view-reader';
            } else if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var('mem_list') ) {
                $classes[] = 'page-members-list';
            } else if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var('rr_list') ) {
                $classes[] = 'page-reading-list';
            }
                
            if ( rh_role('reader') ) $classes[] = 'reader-page';
            if ( !rh_role('reader') ) $classes[] = 'customer-page';

            return $classes;
        }

        public function title() {
            foreach ( $this->init() as $slug => $file ) {
                if ( get_query_var( $slug ) && !is_admin() ) {
                    echo ucwords($file);
                }
            }
        }

        public function access( $user ) {
            $pages = array(
                'reader'    => array(),
            );
        }
 
        public function redirect() {
            $url =  home_url()."{$_SERVER['REQUEST_URI']}";
            $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
            if ( !is_user_logged_in() ) {
                if ( get_query_var('type') || !rh_page('index') )
                header('Location: '. self::url('index'). '?ref='. $escaped_url );
            } else {
                if ( _rh_user_banned( rh_user('ID') ) && !rh_page('index') ) {
                    header('Location: '. self::url('index') );
                }
                if ( rh_role('poster') && !_rh_user_banned( rh_user('ID') ) )  {
                    if( 
                        rh_page('index') || 
                        rh_page('request') || 
                        rh_page('reading-view') ||
                        rh_page('reader-profile') ) 
                        return;
                    header('Location: '. self::url('request') );
                }
                if ( rh_page('index') && rh_role('reader') )  {
                    //header('Location: '. self::url('dashboard') );
                }
            }
        }
		
        public static function activate() {
            self::rewrite();
            flush_rewrite_rules();
        }

        public static function url( $slug ) {
            return get_bloginfo('url'). '/' .array_search( $slug, self::$slug);  
        }
    }
    new RH_Rewrite;
}