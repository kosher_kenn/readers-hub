<?php
/**
 * Package Included for Readers Hub
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  URL Rewrite Handles
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

// Duplication is not allowed
if( !class_exists( 'RH_Reading' ) ) {

    class RH_Reading {

        public $status_key = '_rh_status';
        public $assign_key = '_rh_assign_to';
        public $type = 'readers_hub'; 

        public function __construct() {

            //add_action('admin_init', array( $this, 'redirect_poster' ) );
            add_action( 'wp_ajax_rh_readings', array( $this, 'ajax' ) );
            add_action( 'rh/head', array( $this, 'list_users' ) );
            
            add_shortcode( 'rh_request_form', array( $this, 'request_form') );
            add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
            add_action( 'wp_footer', array( $this, 'force_role' ) );
            add_action( 'admin_footer', array( $this, 'force_role' ) );
            //add_action( 'rh/head', array( $this, 'user' ) );
            
            add_action('set_current_user', array( $this, 'set_current_user'), 1 );
        } 

        public function force_role() {
            $this->roles_caps();
        }
        
        public function set_current_user() {
            $user = wp_get_current_user();
            if ( in_array( 'ftr_user', $user->roles ) ) {
                $current = new WP_User( $user->ID );
                remove_action( 'bbp_setup_current_user', 'bbp_set_current_user_default_role' );
                $current->remove_cap( 'wp_communitycapabilities' );
            }
        }

        public function ajax() {
            check_ajax_referer( 'rh_page_req', 'security' );

            $response['status'] = false;

            if ( isset( $_POST['rh_req'] ) ) {
                if ( $_POST['rh_req'] === 'new_readings' ) {
                    $rand_user = rh_reader_select(true);
                    if ( $rand_user ) {
                        $readings = array(
                            'post_title'    => 'New Readings From '. rh_user('display_name'). ' Posted on '. current_time('F j g:ia Y'),
                            'post_content'  => $_POST['rh_content'],
                            'post_status'   => 'publish',
                            'post_type'       => $this->type,
                        );
                        $readings_id = wp_insert_post( $readings );
                        update_post_meta( $readings_id, $this->status_key, 'assigned');
                        update_post_meta( $readings_id, $this->assign_key, $rand_user );
                        update_post_meta( $readings_id, '_rh_assign_date', current_time( 'mysql' ) );
                        $response = array(
                            'status'    => 'Message Successfuly Posted',
                            'id'        => $readings_id,
                            'meta'      => $read_open,
                        );
                        $link = RH_Rewrite::url('readings').'/'.$readings_id.'/';
                        $subjreader = '[FTR] You have a new request for a Tarot reading';
                        $bodyreader =  $this->mail_body('new_posted_reader', rh_reader_data('name', $rand_user, false), rh_reader_data('name'), $link, self::due_date( $readings_id ), $_POST['rh_content'] );  
                        $subjcust = 'Confirming your request for a Tarot reading';
                        $bodycust = $this->mail_body('new_posted_customer', rh_reader_data('name', $rand_user, false), rh_reader_data('name'), $link, self::due_date( $readings_id ), $_POST['rh_content'] );  
                        $this->email( rh_reader_data('mail', $rand_user, false), $subjreader, $bodyreader );   
                        $this->email( rh_reader_data('mail'), $subjcust, $bodycust );   
                    } else {
                        $response = array(
                            'status'    => 'Sorry, the last available reader is no longer available',
                        );
                    }
                    
                } else if ( $_POST['rh_req'] === 'insert_response' ) {
                    $id = $_POST['rh_ID'];
                    $poster = $this->poster_data( $id );
                    $reader = $this->reader_data( $id );
                    $link = RH_Rewrite::url('readings').'/'.$id.'/';
                    $draft = RH_Reading::get_message( $id, 'rh_draft' );
                    if ( !in_array( rh_user('ID'), self::can_message($id) ) ) wp_send_json( $response );
                    $message = array(
                        'comment_post_ID' => $id,
                        'comment_author' => rh_user('user_nicename'),
                        'comment_author_email' => rh_user('user_email'),
                        'comment_content' => $_POST['rh_content'],
                        'comment_type' => 'rh_message',
                        'comment_parent' => 0,
                        'user_id' => rh_user('ID'),
                        'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
                        'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
                        'comment_date' => current_time('mysql'),
                        'comment_approved' => -1,
                    );
                    if ( $_POST['rh_type'] === 'send-reading' ) {
                        $meta = update_post_meta( $id, $this->status_key, 'awiting_feedback');
                        $stat = add_comment_meta( wp_insert_comment( $message ), 'rh_ctype', 'readings'); 
                    } else {
                        if ( rh_role('reader') ) {
                            $meta = ( self::read_meta('status', $id ) === 'assigned' ) ?
                            update_post_meta( $id, $this->status_key, 'inprogress') : 'noneed' ;
                        }
                        
                        $stat = wp_insert_comment( $message );
                    }
                    if ( rh_role('reader') ) {
                        if ( $draft ) {
                            foreach ($draft as $pos => $item) {
                                wp_delete_comment( $item->comment_ID, true );
                            }                            
                        }
                        update_post_meta( $id, '_reading_open', 'yes');
                        $to = rh_reader_data('mail', $poster->ID);
                        $subject =  '[Free Tarot Readings] You have a new message'; 
                        $body    =  $this->mail_body('reader_posted', $reader->display_name, $poster->display_name, $link);
                    } else if ( rh_role('poster') ) {
                        update_post_meta( $id, '_reading_open', 'no');
                        $to = rh_reader_data('mail', $reader->ID);
                        $subject =  '[FTR] You have a new message'; 
                        $body    =  $this->mail_body('poster_posted', $reader->display_name, $poster->display_name, $link); 
                    } 
                    $this->email( $to, $subject, $body );
                    
                    $response = array(
                        'status'    => $stat,
                        'id'        => $id,
                        'meta'      => $meta,
                        'comment'   => '<span class="user-img">'.rh_reader_data('imager') .'</span><div class="msg-cont">'. $_POST['rh_content'].'</div>',
                    );
                } else if ( $_POST['rh_req'] === 'insert_feedback' ) {
                    $id = $_POST['rh_ID'];
                    $poster = $this->poster_data( $id );
                    $reader = $this->reader_data( $id );
                    if ( $poster && (int) $poster->ID == get_current_user_id() ) {
                        $message = array(
                            'comment_post_ID' => $id,
                            'comment_author' => rh_user('user_nicename'),
                            'comment_author_email' => rh_user('user_email'),
                            'comment_content' => $_POST['feedback'],
                            'comment_type' => 'rh_feedback',
                            'comment_parent' => 0,
                            'user_id' => rh_user('ID'),
                            'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
                            'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
                            'comment_date' => current_time('mysql'),
                            'comment_approved' => -1,
                        );
                        $cmeta = add_comment_meta( wp_insert_comment( $message ), 'rh_rating', $_POST['score']); 
                        update_post_meta( $id, '_reading_open', 'no');
                                       
                        $link = RH_Rewrite::url('readings').'/'.$id.'/';
                        $subject =  '[FTR] You have new feedback';  
                        $body    =  $this->mail_body('feedback_posted', $reader->display_name, $poster->display_name, $link);  
                        $this->email( rh_reader_data('mail', $reader->ID), $subject, $body );    

                        $response = array(
                            'status'    => 'Feedback Successfuly Posted!',
                            'id'        => $cmeta,
                            'meta'      => update_post_meta( $id, $this->status_key, 'closed') ,
                        );             
                    } else {
                        $response['status'] = 'You are not allowed to do this!';
                    }
                } else if ( $_POST['rh_req'] === 'load_comment' ) {
                    $id = $_POST['rh_ID'];
                    $messages = RH_Reading::get_message( $id );
                    $get_draft = RH_Reading::get_message( $id, 'rh_draft' );
                    $draft = ( $get_draft ) ? $get_draft['0']->comment_content : '' ;
                    $response = array(
                        'status'    => true,
                        'draft'     => $draft,
                    );
                } else if ( $_POST['rh_req'] === 'insert_draft' ) {
                    $id = $_POST['rh_ID'];
                    $get_draft = RH_Reading::get_message( $id, 'insert_draft' );
                    if ( !in_array( rh_user('ID'), self::can_message($id) ) ) wp_send_json( $response );
                    $message = array(
                        'comment_post_ID' => $id,
                        'comment_author' => rh_user('user_nicename'),
                        'comment_author_email' => rh_user('user_email'),
                        'comment_content' => $_POST['rh_content'],
                        'comment_type' => 'rh_draft',
                        'comment_parent' => 0,
                        'user_id' => rh_user('ID'),
                        'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
                        'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
                        'comment_date' => current_time('mysql'),
                        'comment_approved' => -1,
                    );
                    if ( $get_draft ) {
                        $draft = array();
                        $draft['comment_ID'] = $get_draft[0]->comment_ID;
                        $draft['comment_content']  = $_POST['rh_content'];                 
                        $stat =  wp_update_comment( $draft );
                    } else {
                        $stat = wp_insert_comment( $message );
                    }
                    $response = array(
                        'status'    => $stat,
                        'id'        => $id,
                    );
                }
            } else if ( isset( $_POST['rh_do'] ) ) {
                $response = array(
                    'status' => 'Successfuly Connected to Database, No data has been updated.'
                );
            }
            wp_send_json( $response );
        }

        public function reader_data( $id ) {
            $reader = self::read_meta( 'assign_to', $id );
            if ( !$reader ) return false;
            $reader_data = get_userdata( $reader );
            return $reader_data;
        }

        public function poster_data( $id ) {
            $poster = get_post_field( 'post_author', $id );
            if ( !$poster ) return false;
            $poster_data = get_userdata( $poster );
            return $poster_data;
        }

        public static function args_modify( $pairs, $atts ) {
            $atts = (array)$atts;
            $out = $pairs;
            if ( !$atts ) return $out;
            foreach ($atts as $name => $key ) {
                $out[$name] = $key;
            }
            return $out;
        }

        public static function read_meta( $key, $id = null ) {
            $i = ( $id ) ? $id : get_the_ID() ;
            return get_post_meta( $id, '_rh_'.$key, true );
        }

        public static function list_users( $available = false ) {
            $users = get_users();
            $data = array();
            foreach ( $users as $user => $datas ) {
                if ( rh_role('reader', $datas->ID) /* && !in_array('administrator', $datas->roles)*/ ) {
                    if ( $available ) {
                        if ( rh_reader_available( $datas->ID ) ) {
                            $data[ $datas->data->ID ] = rh_reader_data('name', $datas->ID, false);
                        }
                    } else  {
                        $data[ $datas->data->ID ] = rh_reader_data('name', $datas->data->ID, false);
                    }
                }
            }
            return $data;
        }

        public static function list_reader() {
            
        }

        public static function status( $stat = null, $return = true ) {
            $statuses = array(
                'open' => __( 'Unstarted', 'trh' ),
                'assigned'   => __( 'FollowUp', 'trh' ),
                'inprogress'     => __( 'In Progress', 'trh' ),
                'awiting_feedback'     => __( 'Awaiting Feedback', 'trh' ),
                'closed'     => __( 'Archive', 'trh' ),
            );
            //if ( $stat && array_key_exists($stat, $statuses) ) return $statuses[$stat];
			if ( $stat && isset( $statuses[$stat] ) ) return $statuses[$stat];
            if ( $return ) return $statuses;
            return false;
        } 

        public static function get_message( $id, $type = "rh_message" ) {
            global $wpdb;
            $query = "SELECT * FROM $wpdb->comments 
                WHERE comment_post_ID = $id
                AND comment_type = %s
                ORDER BY $wpdb->comments.comment_date ASC";
            $messages = $wpdb->get_results( $wpdb->prepare( $query, $type, OBJECT) );
            return $messages;
        }

        public static function reader( $id, $by = 'login' ) { 
            $reader = get_user_by( $by, $id );
            if ( $reader ) return $reader;
            return false;
        }

        public static function can_message( $id ) {
            $post =  get_post( $id );
            $can = array($post->post_author, get_post_meta($id, '_rh_assign_to', true)  );
            return $can;
        }

        public static function email($to, $sub, $mes) {

            add_filter('wp_mail_from_name', function($name) {
                return 'Biddy Tarot Community';
            });
            
            $headers = "MIME-Version: 1.0\r\n" .
            "From: community@biddytarot.com\r\n" .
            "Content-Type: text/html; charset=\"" . get_option('blog_charset') . "\"\r\n";
            
            $message = $mes."\r\n";
            return wp_mail( $to, $sub, $message, $headers );
        }

        public static function mail_body( $template, $reader=null, $poster=null, $link=null, $due=null, $question=null, $login=null, $password = null ) {

            $tags = array(
                '{reader_name}' => $reader,
                '{poster_name}' => $poster,
                '{reading_url}' => $link,
                '{due_date}'    => $due,
                '{question}'    => $question,
                '{login}'       => $login,
                '{password}'    => $password,
            );

            $body = array(
                'feedback_posted'           => strtr( self::re( 'emails/feedback-posted' ), $tags ),
                'reader_posted'             => strtr( self::re( 'emails/reader-response-posted' ), $tags ),
                'poster_posted'             => strtr( self::re( 'emails/poster-response-posted' ), $tags ),
                'new_posted_customer'       => strtr( self::re( 'emails/reading-posted-customer' ), $tags ),
                'new_pc_nonloggedin'        => strtr( self::re( 'emails/reading-posted-customer-new' ), $tags ),
                'new_posted_reader'         => strtr( self::re( 'emails/reading-posted-reader' ), $tags ),
                'reassign_customer'         => strtr( self::re( 'emails/reading-reassign-customer' ), $tags ),
                'reassign_old_reader'       => strtr( self::re( 'emails/reading-reassign-old-reader' ), $tags ),
                'reassign_new_reader'       => strtr( self::re( 'emails/reading-reassign-new-reader' ), $tags ),
                'request_feedback'          => strtr( self::re( 'emails/request-feedback' ), $tags ),
            );
            return $body[ $template ];
        }
        
        public static function re( $temp ) {
            ob_start();
            rh_get_template( $temp );
            return ob_get_clean();
        }

        public static function counter( $data, $before=null, $after=null, $id=null, $zero =false ) {
            $user = ( $id ) ? $id : rh_user('ID');
            $type = array(

                'open'  => array(
                    'meta_key'          => '_rh_assign_to',
                    'meta_value'        => $user,
                    'meta_query' => array(
                        'key' => '_rh_status',
                        'value' => 'assigned',
                        'compare' => '=',
                    ),
                ),
                'in-progress' => array(
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => '_rh_status',
                            'value' => 'inprogress',
                            'compare' => '=',
                        ),
                        array(
                            'key' => '_reading_open',
                            'value' => 'no',
                            'compare' => '=',
                        )
                    ),
                ),
                'waiting-feedback' => array(
                    'meta_query' => array(
                        array(
                            'key' => '_rh_status',
                            'value' => 'awiting_feedback',
                            'compare' => '=',
                        ),
                    ),
                ),
                'archived'  => array(
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => '_rh_status',
                            'value' => 'closed',
                            'compare' => '=',
                        ),
                        array(
                            'key' => '_reading_open',
                            'value' => 'no',
                            'compare' => '=',
                        )
                    )
                ),
                'closed'  => array(
                    'meta_key'          => '_rh_assign_to',
                    'meta_value'        => $user,
                    'meta_query' => array(
                        array(
                            'key' => '_rh_status',
                            'value' => 'closed',
                            'compare' => '=',
                        )
                    )
                ),
            );

            $readings = get_reading( $type[ $data ] );
            if ( !$readings && $zero ) return $before. 0 .$after;
            if ( !$readings && !$zero ) return false;
            return $before. sizeof( $readings ) .$after;
        }

        public static function total() {
            return self::counter('open') + self::counter('in-progress') + self::counter('archived');
        }

        public static function average( $id = null ) {
            $user = ( $id ) ? $id : rh_user('ID');
            if ( !$user ) return false;
            $closed = array(
                'meta_key'          => '_rh_assign_to',
                'meta_value'        => $user,
                'meta_query' => array(
                    array(
                        'key' => '_rh_status',
                        'value' => 'closed',
                        'compare' => '=',
                    )
                )
            );
            $readings = get_reading( $closed );
            if ( !$readings ) return 0;
            $rating = array();
            foreach ($readings as $reading => $data) {
                $rating[] = _rh_get_star_rating( $data->ID ); 
            }
            return array_sum($rating) / count($rating);
        }

        public static function roles() {
            $caps = array( 
                'read'  => true, 
            );
            add_role( 'ftr_reader', 'FTR Reader', $caps );
            add_role( 'ftr_user', 'FTR Customer', $caps );
        }

        public function redirect_poster() {
            $user = wp_get_current_user();
            if ( is_admin() && ( rh_role('poster') || ( rh_role('reader') && !in_array('administrator', $user->roles)) ) 
                && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
                wp_redirect( RH_Rewrite::url('index') ); exit;
            }
        }
        public static function due_date( $id = false, $showall = false ) {
            $assign_date = get_post_meta( $id, '_rh_assign_date',true);
            if ( !$assign_date ) return 'Not Available';
            $now = new DateTime( current_time('mysql') );
            $future =  new DateTime( $assign_date );
            $future->modify('+7 day');
            $t = $future->diff( $now );
            return $future->format('d-F-y');
            if ( $showall ) return $t->format("%a days, %h hours, %i minutes, %s seconds");
            if ( $t->d > 0 ) return sprintf( _n( '%s day', '%s days', $t->d, 'trh' ), $t->d ); 
            if ( $t->d < 1 && $t->h > 0 ) return sprintf( _n( '%s hour', '%s hours', $t->h, 'trh' ), $t->h );
            if ( $t->d < 1 && $t->h < 1 ) return sprintf( _n( '%s minute', '%s minutes', $t->i, 'trh' ), $t->i );
            if ( $t->d < 1 && $t->h < 1 && $t->i < 1 ) return sprintf( _n( '%s second', '%s seconds', $t->s, 'trh' ), $t->s );
            if ( $t->d < 1 && $t->h < 1 && $t->i < 1 && $t->s < 1 ) return 'Overdue';
        }

        public function roles_caps() {
            if ( !user_is_rh() ) return false;

            if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
                global $rh_page;
                
                $user = new WP_User( rh_user('ID') );    

                if ( rh_role('poster') ) $user->remove_role( 'bbp_participant' );

                if ( $rh_page ) {
                    $user->add_cap( 'edit_published_posts' );
                    $user->add_cap( 'delete_posts' );
                    $user->add_cap( 'edit_posts' );
                    $user->add_cap( 'upload_files' );
                    $user->add_cap( 'edit_others_posts' );
                } else if ( !$rh_page ) {
                    $user->remove_cap( 'edit_published_posts' );
                    $user->remove_cap( 'delete_posts' );
                    $user->remove_cap( 'edit_posts' );
                    $user->remove_cap( 'upload_files' );
                    $user->remove_cap( 'edit_others_posts' );
                }
            }
        }

        public function scripts() {
            global $post;
            if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'rh_request_form') && !is_admin() ) {
                wp_enqueue_style('rh-css', RH_URL .'assets/js/select2/css/select2.min.css');
                wp_enqueue_style('rh-ui-css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
                wp_enqueue_script('rh-select', RH_URL .'assets/js/select2/js/select2.min.js', array('jquery'), '', true );
                wp_enqueue_script('jquery-ui-datepicker');
                wp_enqueue_script('rhs-js', RH_URL .'assets/js/rh-reading-list.js', array('jquery'), '', true );
                wp_enqueue_script('rcaptcha', '//www.google.com/recaptcha/api.js', array('jquery'), '', true );
            }
        }

        public function pre( $val ) {
            if ( isset( $_POST[$val] ) ) return $_POST[ $val ];
            else return '';
        }

        public function new_user_form() {
            $form = '';
            $class = ( !isset( $_POST['action'] ) ) ? ' rhfrom-data': '' ;
            $debug = ( isset( $_GET['rh_debug'] ) ) ? '<input type="hidden" name="rhdebug" value="yes">': '';
            if ( !isset( $_POST['action'] ) ) {
                $form .= '<div class="showforms"><button data-div=".rh-login-form">Login</button><button data-div=".rh-new-request">Register</button></div>';
                $form .= '<div class="rh-login-form rhfrom-data"><p>Before you can request your free Tarot reading, please log in below.</p>'.
                wp_login_form( array(
                    'echo'           => false,
                    //'redirect'       => home_url(). '/readers-hub/',
                    'form_id'        => 'loginform',
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'id_remember'    => 'rememberme',
                    'id_submit'      => 'wp-submit',
                    'label_username' => __( 'email' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in'   => __( 'Log In' ),
                    'value_username' => '',
                    'value_remember' => false
                )).'<a target="_blank" href="'.wp_lostpassword_url().'" title="Lost Password">Lost Password</a></div>';
            }         

            $form .= '<div class="rh-new-request'.$class.'"><form id="create_new_customer" class="rh-form" method="POST" enctype="multipart/form-data">';
            $form .= '<div class="img-section"><img id="rh-plch" src="'.RH_URL.'assets/img/user-img-placeholder.png" alt="User Photo"/><a href="#" id="upload_photo">Upload Your Photo</a><input name="user_img" type="file" id="rh-img" value="'.$this->pre('user_img').'" multiple="false"/></div>';
            $form .= '<div class="field-section"><p><input name="user_name" type="text" placeholder="Name" required value="'.$this->pre('user_name').'"></p>';
            $form .= '<p class="input-half"><input id="user_email" name="user_email" type="email" placeholder="Email" value="'.$this->pre('user_email').'" required>';
            $form .= '<input type="email" placeholder="Re-type your Email" value="'.$this->pre('user_email').'" required></p>';
            $form .= '<p><label>Birthdate</label><input id="bdate" type="text" placeholder="Birth Date" name="user_birtday" value="'.$this->pre('user_birtday').'" required></p>';
            $form .= '<p class="input-half"><span class="rh-half">'.RH_Help::country( $this->pre('rh_country') ).'</span>';
            $form .= '<span class="rh-half last"><select id="user-gender" name="user_gender"><option value="Female">Female</option><option value="Male">Male</option></select></span></p>';
            $form .= '<p><textarea rows="10" name="rh_content" placeholder="What\'s you question? Please provide some detail about your question or situation (Up to 1000 characters)" required>'.$this->pre('rh_content').'</textarea></p>';
            $form .= '<div class="verify"><div class="ver-left"><input type="checkbox" id="tos" value="no"><p>I Understand and agree that, in exchange for a free Tarot Readings. I am obligated to provide detailed and constructive feedback to my allocated Tarot reader. I also agree to the "rules" as outlined above.<br>Your personal details are kept private and confidential, and will only be seen by the Biddy Tarot team and your allocated reader.</p></div>';
            $form .= '<div class="ver-right"><div class="g-recaptcha" id="rcaptcha" style="margin-left: 90px;" data-sitekey="6Lc29iETAAAAAIQMPIFwLBhZGXSmSH_5bu2SnZDb" data-callback="rh_captcha"></div><span id="captcha" style="margin-left:100px;color:red"></span><input id="rh-submit" type="submit" value="Submit Your Request" disabled></div></div>';
            $form .= '</div>';
            $form .= '<input type="hidden" value="no" id="verified" />';
            $form .= '<input type="hidden" value="" id="age_count" />';
            $form .= '<input type="hidden" name="security" value="'.wp_create_nonce('rh_page_req').'">';
            $form .= '<input type="hidden" name="action" value="new_customer_readings">'.$debug;
            $form .= '<script>function rh_captcha() {var tos = document.getElementById("tos").value; document.getElementById("verified").value = "yes"; if ( tos != "no" ) document.getElementById("rh-submit").disabled = false;}</script>';
            return $form. '</form></div>' ;
        }

        public function login_user_form() {
            $form = '<div class="rh-new-request"><form id="create_new_customer" class="rh-form" method="POST">';
            $form .= '<p><textarea rows="10" name="rh_content" placeholder="What\'s you question? Please provide some detail about your question or situation (Up to 1000 characters)" required>'.$this->pre('rh_content').'</textarea></p>';
            $form .= '<div class="verify"><div class="ver-left"><input type="checkbox" id="tos" value="no"><p>I Understand and agree that, in exchange for a free Tarot Readings. I am obligated to provide detailed and constructive feedback to my allocated Tarot reader. I also agree to the "rules" as outlined above.<br>Your personal details are kept private and confidential, and will only be seen by the Biddy Tarot team and your allocated reader.</p></div>';
            $form .= '<div class="ver-right"><div class="g-recaptcha" id="rcaptcha" style="margin-left: 90px;" data-sitekey="6Lc29iETAAAAAIQMPIFwLBhZGXSmSH_5bu2SnZDb" data-callback="rh_captcha"></div><span id="captcha" style="margin-left:100px;color:red"></span><input id="rh-submit" type="submit" value="Submit Your Request" disabled></div></div>';
            $form .= '</div>';
            $form .= '<input type="hidden" value="no" id="verified" />';
            $form .= '<input type="hidden" name="security" value="'.wp_create_nonce('rh_page_req').'">';
            $form .= '<input type="hidden" name="action" value="loggedin_customer_readings">';
            $form .= '<script>function rh_captcha() {var tos = document.getElementById("tos").value; document.getElementById("verified").value = "yes"; if ( tos != "no" ) document.getElementById("rh-submit").disabled = false;}</script>';
            return $form. '</form></div>' ;
        }
        public function request_form() {
            $message = '';
            if ( !isset( $_POST['action'] ) ) $message = '<p align="center"><span class="gold">There are currently </span><span class="rhu-count">'.count( self::list_users( true ) ). ' free Tarot Readings Available.</span><br>To request a free Tarot reading:</p>';
            if ( isset( $_GET['rh_debug'] ) ) return $message. $this->new_request_response();
            if ( !rh_reader_select() ) 
                return '<p class="errors">We\'re sorry - there are no free Tarot readings available right now.<p>';
            if ( is_user_logged_in() ) {
                if ( !rh_role('poster') || _rh_user_banned( rh_user('ID') ) ) 
                    return '<p class="errors">You are not allowed to post Reading Request.</p>';
                if ( !rh_customer_can_post( rh_user('ID') ) ) 
                    return '<p class="errors">You can only request a reading once a month, Please return after one month has passed.</p>';
                return $message. $this->login_request_response();
            } else {
                if ( _rh_ip_banned() ) return '<p class="errors">You are not allowed to post Reading Request.</p>';
                return $message. $this->new_request_response();
            }
        }

        public function login_request_response() {

            if ( isset( $_POST['action'] ) && $_POST['action'] === 'loggedin_customer_readings' ) {

                wp_verify_nonce( $_POST['security'], 'rh_page_req' );

                $selected_reader = (int) rh_reader_select(true);

                if ( $selected_reader && rh_reader_available( $selected_reader ) ) {
                    $readings = array(
                        'post_title'    => 'New Readings From '. rh_user('display_name') . ' Posted on '. current_time('F j g:ia Y'),
                        'post_content'  => $_POST['rh_content'],
                        'post_status'   => 'publish',
                        'post_type'     => $this->type,
                    );

                    $readings_id = wp_insert_post( $readings );
                    update_post_meta( $readings_id, $this->status_key, 'assigned');
                    update_post_meta( $readings_id, $this->assign_key, $selected_reader );
                    update_post_meta( $readings_id, '_rh_assign_date', current_time( 'mysql' ) );

                    $link = RH_Rewrite::url('readings').'/'.$readings_id;

                    $subjreader = '[FTR] You have a new request for a Tarot reading';
                    $bodyreader =  $this->mail_body( 
                        'new_posted_reader', 
                        rh_reader_data('name', $selected_reader, false), 
                        rh_reader_data('name'), 
                        $link, 
                        self::due_date( $readings_id ), 
                        $_POST['rh_content'] 
                    );  

                    $subjcust = 'Confirming your request for a Tarot reading';
                    $bodycust = $this->mail_body(
                        'new_posted_customer', 
                        rh_reader_data('name', $selected_reader, false), 
                        rh_reader_data('name'), 
                        $link, 
                        self::due_date( $readings_id ), 
                        $_POST['rh_content']
                    );  

                    $this->email( rh_reader_data('mail', $selected_reader, false), $subjreader, $bodyreader );   
                    $this->email( rh_reader_data('mail'), $subjcust, $bodycust );  

                    return $this->confirmation( 
                        rh_reader_data('name', $selected_reader, false),
                        rh_reader_data('imager', $selected_reader, false),
                        rh_reader_data('bio', $selected_reader, false) 
                    );
                } else {
                    return '<p class="errors">We\'re sorry - there are no free Tarot readings available right now.</p>';
                }
            } else {
                return $this->login_user_form();
            }
        }
        public function new_request_response() {
            if ( isset( $_POST['action'] ) && $_POST['action'] === 'new_customer_readings' ) {

                wp_verify_nonce( $_POST['security'], 'rh_page_req' );

                $selected_reader = (int) rh_reader_select(true);
                $birthday = DateTime::createFromFormat('M d, Y', $_POST['user_birtday'] );
                $diff = $birthday->diff(new DateTime());
                if ( $diff->y < 18 ) return '<p class="errors">You need to be 18+ to request a free Tarot reading</p>'.$this->new_user_form();
                if ( isset( $_POST['rhdebug'] ) && $_POST['rhdebug'] === 'yes' ) {
                    $ncdata = array(
                        'display_name'  => $_POST['user_name'],
                        'first_name'    => $_POST['user_name'],
                        'user_login'    => $_POST['user_email'],
                        'user_pass'     => wp_generate_password(8),
                        'user_email'    => $_POST['user_email'],
                        'role'          => 'ftr_user',
                    );
                    $cust = wp_insert_user( $ncdata );
                    if ( is_wp_error( $cust ) ) return 'Problem Creating User, Make sure email is not already used!';
                    $tbody = $this->mail_body(
                            'new_pc_nonloggedin', 'No Reader', 
                            $_POST['user_name'], 
                            'No Link', 
                            'due dute',
                            $_POST['rh_content'], 
                            $_POST['user_email'], 
                            $ncdata['user_pass'] 
                        );  
                    $this->email( $_POST['user_email'], 'Test Customer Creation', $tbody );
                    return 'Test User Created <br>user:'.$_POST['user_email'].'<br>password:'.$ncdata['user_pass'];  
                }

                if ( $selected_reader && rh_reader_available( $selected_reader ) ) {

                    $new_customer_data = array(
                        'display_name'  => $_POST['user_name'],
                        'first_name'    => $_POST['user_name'],
                        'user_login'    => $_POST['user_email'],
                        'user_pass'     => wp_generate_password(8),
                        'user_email'    => $_POST['user_email'],
                        'role'          => 'ftr_user',
                    );

                    $customer_id = wp_insert_user( $new_customer_data );

                    if ( ! is_wp_error( $customer_id ) ) {

                        //global $current_user;

                        //$current_user = get_user_by( 'id', $customer_id );
                        //wp_set_auth_cookie( $customer_id, true );

                        //if ( isset( $_POST['user_img'] ) ) {
                            require_once( ABSPATH . 'wp-admin/includes/image.php' );
                            require_once( ABSPATH . 'wp-admin/includes/file.php' );
                            require_once( ABSPATH . 'wp-admin/includes/media.php' );
                        
                            $atts_data = array(
                                'post_author'   => $customer_id,
                            );

                            $attachment_id = media_handle_upload( 'user_img', 0, $atts_data );
                            if ( !is_wp_error( $attachment_id ) ) { 
                                update_user_meta( $customer_id, '_rh_display_uava', wp_get_attachment_url( $attachment_id ) );
                            }
                        //}
                        
                        update_user_meta( $customer_id, '_rh_user_gender', $_POST['user_gender'] );
                        update_user_meta( $customer_id, '_rh_user_location', $_POST['rh_country'] );
                        update_user_meta( $customer_id, '_rh_user_birthday', $_POST['user_birtday'] );

                        $readings = array(
                            'post_title'    => 'New Request From '. $_POST['user_name'] . ' Posted on '. current_time('F j g:ia Y'),
                            'post_content'  => $_POST['rh_content'],
                            'post_status'   => 'publish',
                            'post_type'     => $this->type,
                            'post_author'   => $customer_id,
                        );

                        $readings_id = wp_insert_post( $readings );
                        update_post_meta( $readings_id, $this->status_key, 'assigned');
                        update_post_meta( $readings_id, $this->assign_key, $selected_reader );
                        update_post_meta( $readings_id, '_rh_assign_date', current_time( 'mysql' ) );

                        $link = RH_Rewrite::url('readings').'/'.$readings_id;

                        $subjreader = '[FTR] You have a new request for a Tarot reading';
                        $bodyreader =  $this->mail_body( 
                            'new_posted_reader', 
                            rh_reader_data('name', $selected_reader, false), 
                            $_POST['user_name'], 
                            $link, 
                            self::due_date( $readings_id ), 
                            $_POST['rh_content'] 
                        );  

                        $subjcust = 'Confirming your request for a Tarot reading';
                        $bodycust = $this->mail_body(
                            'new_pc_nonloggedin', 
                            rh_reader_data('name', $selected_reader, false), 
                            $_POST['user_name'], 
                            $link, 
                            self::due_date( $readings_id ), 
                            $_POST['rh_content'], 
                            $_POST['user_email'], 
                            $new_customer_data['user_pass'] 
                        );  

                        $this->email( rh_reader_data('mail', $selected_reader, false), $subjreader, $bodyreader );   
                        $this->email( $_POST['user_email'], $subjcust, $bodycust );  

                        return $this->confirmation( 
                            rh_reader_data('name', $selected_reader, false),
                            rh_reader_data('imager', $selected_reader, false),
                            rh_reader_data('bio', $selected_reader, false) 
                        );

                    } else {
                        $err = array();
                        $errors = $customer_id->errors;
                        foreach ($errors as $errors => $value) {
                            $err[] = '<p class="errors">'.$value[0].'</p>';
                        }
                        return implode('<br>', $err) . $this->new_user_form();
                        //return '<pre>'.print_r($customer_id,1). '</pre>' . $this->new_user_form();
                    }
                } else {
                    return '<p class="errors">We\'re sorry - there are no free Tarot readings available right now.</p>';
                }
            } else {
                return $this->new_user_form();
            }
        }

        public function confirmation( $reader=null, $image=null, $bio=null) {
            return 'Thanks for requesting a free Tarot reading in exchange for feedback.<br><br> Your assigned reader is '.$reader.'.<br><br><span style="float:left;margin: 0 20px 20px 0">'.$image.'</span>'. $bio .'<div style="clear:both"></div>You\'ll receive an email shortly with details on how to access​ your reading and communicate with your reader. Your reader will send your completed reading to you within the next 7 days.';
        }
    }
    new RH_Reading;
}