<?php
/**
 * Package Included for Readers Hub
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  URL Helpers method
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

// Duplication is not allowed
if( !class_exists( 'RH_Help' ) ) {

    class RH_Help {

        public function __construct() {
            add_filter( 'ajax_query_attachments_args', array( $this, 'user_images' ) );
            add_filter( 'rh/head', array( $this, 'ban' ) );
        }

        public function user_images( $query ) {
            $user = wp_get_current_user();
            $id = rh_user('ID');
            if ( ( rh_role('reader') || rh_role('poster') ) && !in_array('administrator', $user->roles ) ) {
                $query['author'] = $id;
            }
            return $query;
        }

        public static function clean( $path ) {
            $path = str_replace('','', str_replace( array( "\\", "\\\\" ), '/', $path ) );
            if ($path[ strlen($path)-1 ] === '/') {
                $path = rtrim($path, '/');
            }
            return $path;
        }

        public static function template_dir() {
        	return apply_filters('rh/template', self::clean( RH_DIR. 'templates' ). '/' );
        }

        public static function timezone( $active = null, $class=null ) {
            $regions = array(
                'Africa' => DateTimeZone::AFRICA,
                'America' => DateTimeZone::AMERICA,
                'Antarctica' => DateTimeZone::ANTARCTICA,
                'Arctic' => DateTimeZone::ARCTIC,
                'Aisa' => DateTimeZone::ASIA,
                'Atlantic' => DateTimeZone::ATLANTIC,
                'Australia' => DateTimeZone::AUSTRALIA,
                'Europe' => DateTimeZone::EUROPE,
                'Indian' => DateTimeZone::INDIAN,
                'Pacific' => DateTimeZone::PACIFIC
            );
            $timezones = array();
            foreach ( $regions as $name => $mask ) {
                $zones = DateTimeZone::listIdentifiers($mask);
                foreach($zones as $timezone) {
                    $time = new DateTime(NULL, new DateTimeZone($timezone));
                    $ampm = $time->format('H') > 12 ? ' ('. $time->format('g:i a'). ')' : '';
                    $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $time->format('H:i') . $ampm;
                }
            }
            $out = '<select id="user-timez" class="'.$class.'">';
            foreach($timezones as $region => $list) {
                $out .= '<optgroup label="' . $region . '">' . "\n";
                foreach($list as $timezone => $name ) {
                    $selected = ( $timezone === $active ) ? ' selected': '' ;
                    $out .='<option value="' . $timezone . '"'.$selected.'>' . $name . '</option>' . "\n";
                }
                $out .= '<optgroup>' . "\n";
            }
            return $out.'</select>';
        }

        public static function country( $active=false, $class='', $arg=false ) {
            $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
            $list = array();
            $out = '<select name="rh_country" id="rh-country" class="'.$class.'">';
            $out .= ( $active ) ? '': '<option value="" disabled selected>Country</option>';
            foreach ($countries as $country => $name ) {
                $list[ $name ] = $name;
                $selected = ( $name == $active ) ? ' selected': '';
                $out .= '<option value="' . $name . '"'.$selected.'>' . $name . '</option>' . "\n";
            }
            if ( $arg ) return $list;
            return $out.'</select>';
        }

        public function ban() {

            $ban = get_option('rh_bannedIP');
            $bannedIP = $ban && isset( $ban['_rh_banned_ips'] ) ? explode("\n", $ban['_rh_banned_ips'] ) : array();
            
            if ( _rh_user_banned( rh_user('ID') ) && !_rh_ip_banned() ) $bannedIP[] = _rh_current_ip();
            if ( !_rh_user_banned( rh_user('ID') ) && ( $key = array_search( _rh_current_ip(), $bannedIP ) ) !== false) {
                unset( $bannedIP[$key] );
            }
            update_option( 'rh_bannedIP', array(
                '_rh_banned_ips' => implode("\n", $bannedIP ),
            ));
        }
    }

    $rh_help = new RH_Help;
}