<?php
/**
 * Package Included for Readers Hub Plugin
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  Readers Hub Builder Function
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

// Build initial Template
function rh_template() {
	rh_get_template( 'header' );
	rh_get_template( 'aside' );
	rh_get_template( 'content' );
	rh_get_template( 'footer' );
}

//load side content via hook
add_action('rh/aside', '_rh_aside');
function _rh_aside() { 
	$support_link = ( rh_role('reader') ) ? 'http://community.biddytarot.com/help/' 
		:'http://www.biddytarot.com/contact-faq/';	?> 
			<a href="http://community.biddytarot.com/" class="back-to">Back to Membership</a>
			<div class="logo"><a data-page="index" href="<?php echo RH_Rewrite::url('index'); ?>"><img src="<?php echo RH_URL; ?>assets/img/TRH-Logo.png" title="Biddy Tarot"/><span class="tagline">Tarot Reader Hub</span></a></div>

			<?php if ( is_user_logged_in() ) { echo _rh_main_nav(); ?>
			<ul class="side-nav second">
				<li class="support"><a target="_blank" href="<?php echo $support_link; ?>">Support</a></li>
				<li class="logout"><a href="<?php echo wp_logout_url( RH_Rewrite::url('index') ); ?>">Logout</a></li>
			</ul><?php }
}

function _rh_main_nav() {
	if ( _rh_user_banned( rh_user('ID') ) ) return false;
	$out = '<ul class="side-nav first">';
	if ( rh_role('poster') ) {
		$nav = array(
			'request' => array(
				'label' => 'Readings Request',
				'url'	=> RH_Rewrite::url('request'),
			),
		);
	} else if ( rh_role('reader') ) {
		$nav = array(
			'dashboard' => array(
				'label' => 'Dashboard',
				'url'	=> RH_Rewrite::url('dashboard'),
			),
			'start'	=> array(
				'label' => 'New? Start Here',
				'url'	=> RH_Rewrite::url('start'),
			),
			'profile' => array(
				'label'	=> 'Profile',
				'url' 	=> RH_Rewrite::url('profile'),
			),
			'readings' => array(
				'label'	=> 'Readings',
				'url'	=> RH_Rewrite::url('readings'),
			),		
			'availability' => array(
				'label'	=> 'Availability',
				'url'	=> RH_Rewrite::url('availability'),
			), 
		);
	} else {
		$nav = array();
	}
	if ( rh_role('admin') ) { 
		$nav['members-list'] = array(
			'label'	=> 'Admin Panel',
			'url'	=> RH_Rewrite::url('members-list'),
		);	
	}
	foreach ( $nav as $key => $val ) {
		$class = ( $key === rh_page() ) ? $key. ' active' : $key;
		$count = ( $key === 'readings' && /*$key !== rh_page() &&*/ RH_Reading::total() ) 
			? '<span class="rhctr">'.RH_Reading::total().'</span>': '';
		$out .= '<li class="'.apply_filters('rh/nav/cl', $class).'"><a data-page="'.$key.'" href="'.$val['url'].'">'.$val['label']. $count .'</a></li>';
	}
	$out .= '</ul>';
	return $out;
}

//load header content via hook
add_action('rh/content', '_rh_header', 1);
function _rh_header() { ?>
<header class="header-area">
	<div class="banner">
		<a href="<?php echo RH_Rewrite::url('dashboard'); ?>"><img src="<?php echo RH_URL; ?>assets/img/TRH-FreeTarotReadings.png" title="Biddy Tarot"/></a>
	</div>
	<div class="right-section">
		<?php if ( is_user_logged_in() ): ?>
		<img src="<?php echo rh_reader_data('image'); ?>" alt="<?php echo rh_reader_data('name'); ?>"/>
		<span class="ftr-name"><?php echo rh_reader_data('name'); ?></span>
		<?php if ( rh_role('reader') ) { echo '<span class="rating">'.rh_user_stat( rh_user('ID') ).'</span>'; }
		endif; ?>
	</div>
</header>
<?php	
}

add_action('rh/content', '_rh_content', 5);
//load content via hook
function _rh_content() { ?>
<div class="site-content">
	<div class="wrap">
		<?php do_action('rh/page/heading'); ?>
		<div class="page-content"><?php do_action('rh/page/content'); ?></div>
	</div>
</div><?php
}

//load heading via hook
add_action('rh/page/heading', 'show_rh_heading');
function show_rh_heading() {
	echo '<h2 class="page-heading">'._rh_heading_loader().'</h2>';
}
function _rh_heading_loader( $page = null ) {
	if ( rh_page() === 'index' || $page === 'index' ) 
		return 'Welcome to the Biddy Tarot Free Tarot Readings';
	if ( rh_page() === 'dashboard' || $page === 'dashboard' ) 
		return 'My Dashboard';
	if ( rh_page() === 'start' || $page === 'start' ) 
		return 'Welcome to the Free Tarot Readers Hub';
	if ( rh_page() === 'profile' || $page === 'profile' )
		return 'My Profile';
	if ( rh_page() === 'edit' || $page === 'edit' ) 
		return (rh_role('reader') ) ? 'Edit Profile' : 'Setup Profile';
	if ( rh_page() === 'readings' || $page === 'readings' ) 
		return 'Reading Status';
	if ( rh_page() === 'in-progress' || $page === 'in-progress' ) 
		return _rh_readings_title('inprogress');
	if ( rh_page() === 'waiting-feedback' || $page === 'waiting-feedback' ) 
		return _rh_readings_title('wf');
	if ( rh_page() === 'archived' || $page === 'archived' ) 
		return _rh_readings_title('archived');
	if ( rh_page() === 'availability' || $page === 'availability' ) 
		return 'Your Availability';
	if ( rh_page() === 'request' || $page === 'request' ) 
		return 'My Reading Request';
	if ( get_query_var('type') === 'readers_hub' && get_query_var( 'rh' ) ) 
		return ( rh_role('reader', rh_user('ID') ) ) ? _reader_readings_nav() : 'Reading Status';
	if ( get_query_var('type') === 'readers_hub' && get_query_var( 'rh_username' ) ) 
		return _rh_user_page_title();
}

//load pages contents
add_action('rh/page/content', 'show_rh_content');
function show_rh_content() {
	echo _rh_content_loader();
}

function _rh_user_page_title() {
	$backto = '';
	$referrer = ( isset( $_SERVER["HTTP_REFERER"] ) ) ? $_SERVER["HTTP_REFERER"] : RH_Rewrite::url('request') ;
	if ( rh_role('poster') ) $backto = '<a class="backlink" href="'.$referrer .'">Back To Request</a>'; 
 	return ( RH_Reading::reader( get_query_var( 'rh_username' ), 'ID' ) ) ? $backto. RH_Reading::reader( get_query_var( 'rh_username' ), 'ID' )->display_name. '\'s Profile'
			: 'User Don\'t Exist';
}

function _rh_readings_title( $page ) {
	$inprogress = RH_Reading::counter('in-progress');
	$wf = RH_Reading::counter('waiting-feedback');
	$archived 	= RH_Reading::counter('archived');
	$title = array(
		'inprogress'	=> ( $inprogress) ? 'You have '.$inprogress.' new follow-ups awaiting your answer!': 'In Progress Readings',
		'wf'			=> ( $wf ) ? 'Some of your readings are still awaiting feedback': 'Awaiting Feedback',
		'archived'		=> ( $archived ) ? 'You have '.$archived.' New Users that left you Feedback!': 'Archived Readings',
	); 
	return $title[ $page ];
}

function _reader_readings_nav() {
	$current = get_query_var('rh');
	$next = '';
	$prev = '';
	$status = RH_Reading::read_meta('status', $current);
	$type = array(
		'orderby'	=> 'ID',
		'order'		=> 'ASC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
	            'key' => '_rh_status',
				'value' => 'closed',
				'compare' => '=',
	        ),
	        array(
	            'key' => '_rh_assign_to',
				'value' => rh_user('ID'),
				'compare' => '=',
	        )
		),
	);
	$allreadings = get_reading( $type );
	if ( !$current || $status !== 'closed' ) return false;
	foreach ($allreadings as $reading => $value) {
		$id[]= $value->ID;
	}
	if ( rh_prev_r($id, $current) ) 
		$prev = '<a class="prev" href="'.RH_Rewrite::url('readings').'/'.rh_prev_r($id, $current).'">Previous Reading</a>';
	if ( rh_next_r($id, $current) )
		$next = '<a class="next" href="'.RH_Rewrite::url('readings').'/'.rh_next_r($id, $current).'">Next Reading</a>';
	$nav = '<span class="readings-nav">'.$prev.'<a class="ckise" href="'.RH_Rewrite::url('archived').'">Close Reading</a>'.$next.'</span>';
	return $nav;
}

function _rh_content_loader( $page = null) {
	ob_start();
	$slugs = RH_Rewrite::$slug;
	foreach ( $slugs as $slug => $val ) {
		if( ( rh_page() === $val || $page === $val ) && file_exists( RH_DIR. 'templates/structure/content/'. $val. '.php')  ) {
			rh_get_template( 'content/'. $val );
		}
	}
	if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var( 'rh' ) ) {
			rh_get_template( 'content/view-reading' );
	}
	if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var( 'rh_username' ) ) {
			rh_get_template( 'content/view-reader' );
	}
	/*if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var( 'mem_list' ) ) {
			rh_get_template( 'content/members-list' );
	}
	if ( get_query_var( 'type' ) === 'readers_hub' && get_query_var( 'rr_list' ) ) {
			rh_get_template( 'content/reading-list' );
	}*/
	return ob_get_clean();
}


add_action('wp_ajax_nopriv_rh_new_customer', '_register_new_customer');
function _register_new_customer() {
	check_ajax_referer( 'rh_page_req', 'security' );
	if ( isset( $_POST['user_email'] ) ) {
		$email = $_POST['user_email'];
		$new_customer_data = array(
			'display_name'	=> $_POST['user_name'],
			'first_name'	=> $_POST['user_name'],
		    'user_login' 	=> $email,
		    'user_pass'  	=> wp_generate_password(8),
		    'user_email' 	=> $email,
		    'role'       	=> 'ftr_user',
		);
		$customer_id = wp_insert_user( $new_customer_data );
		if ( ! is_wp_error( $customer_id ) ) {
			update_user_meta( $customer_id, '_rh_user_gender', $_POST['user_gender'] );
			update_user_meta( $customer_id, '_rh_user_location', $_POST['user_location'] );
			wp_new_user_notification( $customer_id, $new_customer_data->user_pass, 'both' );
			$current_user = get_user_by( 'id', $customer_id );
			wp_set_auth_cookie( $customer_id, true );
			wp_send_json( array('url' => RH_Rewrite::url('index') ) );	
		} else {
			wp_send_json( $customer_id );
		}
	}	
}

add_action( 'wp_ajax_rh_page_req', '_rh_page_ajax' );
function _rh_page_ajax() {
	check_ajax_referer( 'rh_page_req', 'security' );
	$response = false;
	$user = wp_get_current_user();
	if ( isset( $_POST['page_req'] ) ) {
		$response = array(
			'title'		=> _rh_heading_loader( $_POST['page_req'] ),
			'content'	=>  _rh_content_loader( $_POST['page_req'] ),
			'page'		=> $_POST['page_req'],
			'subnav'	=> ( $_POST['page_req'] == 'members-list' || $_POST['page_req'] == 'reading-list') 
							? _rh_page_subnav( 'admin' ) : false,
		);	
	} else if ( isset( $_POST['rh_do'] ) && $_POST['rh_do'] === 'profile_save' ) {
		if ( rh_role('reader') || rh_role('canbe_reader') ) {
			//wp_send_json( $_REQUEST );
			$reader = new WP_User( $user->ID );
			if ( isset( $_POST['rh_uname'] ) ) update_user_meta( $user->ID, '_rh_display_uname', $_POST['rh_uname'] );
			if ( isset( $_POST['rh_uava'] ) ) update_user_meta( $user->ID, '_rh_display_uava', $_POST['rh_uava'] );
			if ( isset( $_POST['rh_bio'] ) ) update_user_meta( $user->ID, 'description', $_POST['rh_bio'] );
			if ( isset( $_POST['rh_email'] ) ) update_user_meta( $user->ID, '_rh_user_email', $_POST['rh_email'] );
			if ( isset( $_POST['rh_timez'] ) ) update_user_meta( $user->ID, '_rh_user_timez', $_POST['rh_timez'] );
			if ( isset( $_POST['rh_weekly'] ) ) update_user_meta( $user->ID, '_rh_user_weekly', $_POST['rh_weekly'] );
			if ( isset( $_POST['rh_sd'] ) ) update_user_meta( $user->ID, '_rh_user_seldate', $_POST['rh_sd'] );
			if ( isset( $_POST['rh_leave'] ) ) update_user_meta( $user->ID, '_rh_user_onleave', $_POST['rh_leave'] );
			if ( isset( $_POST['rh_gender'] ) ) update_user_meta( $user->ID, '_rh_user_gender', $_POST['rh_gender'] );
			if ( isset( $_POST['rh_loc'] ) ) update_user_meta( $user->ID, '_rh_user_location', $_POST['rh_loc'] );
			if ( isset( $_POST['rh_leave'] ) ) {
				$response = array(
					'status' => 'Settings Saved! Your Availability has been updated',
				);
			} 
			if ( isset( $_POST['rh_become'] ) && !empty( $_POST['rh_become'] ) && rh_role('canbe_reader') ) {
				$reader = new WP_User( $user->ID );
				$reader->add_role( 'ftr_reader' );
				$response = array(
					'status'	=> 'profile_updated',
					'link'		=> RH_Rewrite::url('dashboard'),
				);
			} else {
				$response = array(
					'status'	=> 'profile_updated',
					'link'		=> RH_Rewrite::url('profile')
				);
			}
		} else {
			$response = $_REQUEST;
		}
		
	} else if ( isset( $_POST['rh_do'] ) && $_POST['rh_do'] === 'rh_mod_admin' ) {
		if ( !rh_role('admin') ) wp_send_json( $response );
		if ( $_POST['rh_mod'] === 'suspend' ) {
			$user = new WP_User( (int) $_POST['rh_user'] );
			$response = array(
				'status' => update_user_meta( (int) $_POST['rh_user'], '_rh_user_suspend', 1),
				'user'	=> $_POST['rh_user'],
				'type'	 => $_POST['rh_mod'],
			);
		} else if ( $_POST['rh_mod'] === 'unsuspend' ) {
			$response = array(
				'status' => update_user_meta( (int) $_POST['rh_user'], '_rh_user_suspend', 0),
				'user'	=> $_POST['rh_user'],
				'type'	 => $_POST['rh_mod'],
			);
		} else if ( $_POST['rh_mod'] === 'remove' ) {
			$user = new WP_User( (int) $_POST['rh_user'] );
			$user->remove_role( 'ftr_reader' );
			$response = array(
				'status' => true,
				'user'	 => $_POST['rh_user'],
				'type'	 => $_POST['rh_mod'],
			);
		} else if ( $_POST['rh_mod'] === 'delete' ) {
			$response = array(
				'type'		=> $_POST['rh_mod'],
				'status'	=> wp_delete_post( $_POST['rh_user'], true ),
				'post'		=> $_POST['rh_user'],
			);
		} else if ( $_POST['rh_mod'] === 'reassign' ) {
			$response = array(
				'status'	=> true,
				'rlist'		=> rh_reader_select(false, null, $_POST['rh_reader']),
			);
		}  else if ( $_POST['rh_mod'] === 'reassign_reading' ) {
			$selected = (int) $_POST['rh_user'];
			$user = ( $selected ) ? $selected : rh_reader_select(true, (int) $_POST['rh_reader']);
			if ( $selected && !rh_reader_available( $selected ) ) {
				$response = array(
					'status'	=> false,
					'notice'	=> 'Reader No Longer Available',
				);
			} elseif ( !$user ) {
				$response = array(
					'status'	=> false,
					'notice'	=> 'Current assign reader is the only reader Available',
				);
			} else {
				$poster = rh_poster_data( $_POST['rh_post'] );
				$name = rh_reader_data('name', $user, false );
				$image = rh_reader_data('imager', $user, false );
				update_post_meta( $_POST['rh_post'], '_rh_status', 'assigned' );
				update_post_meta(  $_POST['rh_post'], '_rh_assign_date', current_time( 'mysql' ) );
				$response = array(
					'status'	=> update_post_meta( $_POST['rh_post'], '_rh_assign_to', $user ),
					'reader'	=> $image.'<i class="name">'.$name.'<u>'.rh_user_stat( $user ).'</u></i>',
					'due_date'	=> RH_Reading::due_date( $_POST['rh_post'] ),
					'notice'	=> 'Reading Successfuly Re-assign to '. $name,
					'rid'		=> $user,
				);
				$link = RH_Rewrite::url('readings').'/'.$_POST['rh_post'].'/';
				$dash = RH_Rewrite::url('dashboard');
				$subjcust = '[Free Tarot Readings] Your reading has been reassigned';
				$subjnewr = '[FTR] You have a new reassigned reading';
				$bodycust = RH_Reading::mail_body('reassign_customer', $name, rh_reader_data('name',$poster->ID, false), $link );
				$bodynewr = RH_Reading::mail_body('reassign_new_reader', $name, rh_reader_data('name',$poster->ID, false), $dash, RH_Reading::due_date( $_POST['rh_post'] ), get_post_field('post_content', $_POST['rh_post']) );

				RH_Reading::email( rh_reader_data('mail',$poster->ID,false), $subjcust, $bodycust ); 
				RH_Reading::email( rh_reader_data('mail',$user,false), $subjnewr, $bodynewr );
				if ( isset( $_POST['rh_reader']) && !empty( $_POST['rh_reader'] )) {
					$subjoldr = '[FTR] Your reading has been reassigned';
					$bodyoldr = RH_Reading::mail_body(
						'reassign_old_reader', 
						rh_reader_data('name', (int) $_POST['rh_reader'], false), 
						rh_reader_data('name',$poster->ID, false), 
						$dash 
					);
					RH_Reading::email( rh_reader_data('mail',$_POST['rh_reader'],false), $subjoldr, $bodyoldr );
				}
			}
		} else {
			$response = $_REQUEST;
		}
	} else if ( isset( $_POST['rh_do'] ) && $_POST['rh_do'] === 'request_feedback' ) {
		$poster = rh_poster_data( $_POST['rh_reading'] );
		$reader = rh_assigned_reader( $_POST['rh_reading'] );
		$link = RH_Rewrite::url('readings').'/'.$_POST['rh_reading'].'/';
		$subj = '[Free Tarot Readings] What did you think? (2 min feedback)';
		$body = RH_Reading::mail_body('request_feedback', 
			rh_reader_data('name', $reader->ID, false), 
			rh_reader_data('name',$poster->ID, false), 
			$link 
		);

		$response = array(
			'status' => RH_Reading::email( rh_reader_data('mail', $poster->ID,false), $subj, $body ),
		);
	} else if ( isset( $_POST['rh_do'] ) && $_POST['rh_do'] === 'move_to_archived' ) {
		$response = array(
			'status' => update_post_meta( $_POST['rh_reading'], '_rh_status', 'closed'),
		);
	} else if ( isset( $_POST['rh_do'] ) ) {
		$response = array(
			'status' => 'Successfuly Connected to Database, No data has been updated.'
		);
	}
	wp_send_json( $response );    
}

add_action('rh/head', '_ajax_script');
function _ajax_script() { 
	$rh_action = '';
	if ( rh_page('index') ) $rh_action = 'new_readings';
?>
<script type='text/javascript'>
	/* <![CDATA[ */
		var rh_ajax = {
			'sec': '<?php echo wp_create_nonce('rh_page_req'); ?>',
			'url': '<?php echo admin_url( 'admin-ajax.php' ); ?>',
			'action' : '<?php echo $rh_action; ?>',

		};
	/* ]]> */
</script>
<?php
}

//load reading top nav 
function _reading_top_nav( $page = null ) {
	ob_start();
	$slugs = RH_Rewrite::$slug;
	foreach ( $slugs as $slug => $val ) {
		if( strpos( $slug, rh_page() ) !== false || $page === $val ) {
			return $top_nav;
		}
	}
	return ob_get_clean();
}

add_action('rh/after/container', '_wp_editor_script');
function _wp_editor_script() {
	#if ( rh_page('index') ||  rh_page('readings') ||  rh_page('edit') ||  rh_page('profile') || get_query_var('rh') ) {
		add_filter('show_admin_bar', '__return_false'); 
		//wp_head();
		wp_footer();
		echo '<script src="'.RH_URL.'assets/js/readings.js"></script>';
	#}

}

add_action('rh/content', '_rh_page_subnav', 2);
function _rh_page_subnav( $page = null ) { 
	$nav = '';
	if( !rh_role('reader') ) return false;
	if ( 
		rh_page('in-progress') ||  
		rh_page('readings') || 
		rh_page('archived') || 
		//get_query_var('rh') || 
		rh_page('waiting-feedback') 
	) { 
		$nav .= '<nav class="nav-wrapper"><ul class="reading-nav">';
		$item = array(
			'readings' => array(
				'url'	=> RH_Rewrite::url('readings'), 
				'label'	=> 'New Requests '. RH_Reading::counter('open', '<span class="rhctr">', '</span>'),
			),
			'in-progress' => array(
				'url'	=> RH_Rewrite::url('in-progress'), 
				'label'	=> 'In Progress '. RH_Reading::counter('in-progress', '<span class="rhctr">', '</span>'),
			),
			'waiting-feedback' => array(
				'url'	=> RH_Rewrite::url('waiting-feedback'), 
				'label'	=> 'Awaiting Feedback',
			),
			'archived' => array(
				'url'	=> RH_Rewrite::url('archived'), 
				'label'	=> 'Archived Readings '. RH_Reading::counter('archived', '<span class="rhctr">', '</span>'),
			),
		);
	} else if ( rh_page('members-list') || rh_page('reading-list') || $page === 'admin') {
		$nav .= '<nav class="nav-wrapper"><ul class="reading-nav admin-nav">';
		$item = array(
			'members-list' => array(
				'url'	=> RH_Rewrite::url('members-list'), 
				'label'	=> 'Free Readers',
			),
			'reading-list' => array(
				'url'	=> RH_Rewrite::url('reading-list'), 
				'label'	=> 'Active Readings',
			),
		);
	} else {
		$nothinhere = '';
	}
	if ( !isset( $item ) ) return false;
	foreach ( $item as $data => $val ) {
		$class = ( $data === rh_page() ) ? $data. ' active' : $data;
		$nav .= '<li class="'.$class.'"><a data-page="'.$data.'" href="'.$val['url'].'">'.$val['label']. '</a></li>';
	} 
	if ( $page && $nav ) return $nav .'</ul></nav>';
	if ( !$page && $nav ) echo $nav .'</ul></nav>';
}

add_action('rh/content/classes', '_rh_content_classes');
function _rh_content_classes( $id ) {
	if ( !$id ) return;
	$data = RH_Reading::read_meta('status', $id->ID );
	if ( $data === 'closed' ) echo 'closed-reading';
}

add_action('rh/head', array('RH_Reading', 'average') );

function rh_nav_classes_filters( $cl ) {
	if( strpos( $_SERVER['REQUEST_URI'], $cl ) !== false  ) {
		$cl = $cl. ' active';
	}
	if( strpos( $_SERVER['REQUEST_URI'], 'admin') !== false && $cl === 'members-list' ) {
		$cl = $cl. ' active';
	}
	return $cl;
}
add_filter('rh/nav/cl', 'rh_nav_classes_filters');
