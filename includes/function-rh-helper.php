<?php
/**
 * Package Included for Readers Hub Plugin
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  Readers Hub Locate Template Functions
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

function rh_get_template( $slug, $name = null, $load = true ) {

	do_action( 'get_template_part_' . $slug, $slug, $name );
 
	$templates = array();
	if ( isset( $name ) ) $templates[] = $slug . '-' . $name . '.php';
	$templates[] = $slug . '.php';

	$templates = apply_filters( 'rh_get_template', $templates, $slug, $name );

	return rh_locate_template( $templates, $load, false );
}

function rh_locate_template( $template_names, $load = false, $require_once = true ) {

	$located = false;

	foreach ( (array) $template_names as $template_name ) {

		if ( empty( $template_name ) ) continue;

		$template_name = ltrim( $template_name, '/' );

		if ( file_exists( RH_Help::clean( get_stylesheet_directory() ) . 'readers-hub/' . $template_name ) ) {
			$located = RH_Help::clean( get_stylesheet_directory() ) . 'readers-hub/' . $template_name;
			break;

		} elseif ( file_exists( RH_Help::clean( get_template_directory() ) . 'readers-hub/' . $template_name ) ) {
			$located = RH_Help::clean( get_template_directory() ) . 'readers-hub/' . $template_name;
			break;

		} elseif ( file_exists( RH_Help::template_dir() . $template_name ) ) {
			$located = RH_Help::template_dir() . $template_name ;
			break;
		} elseif ( file_exists( RH_Help::template_dir() . 'structure/' . $template_name ) ) {
			$located = RH_Help::template_dir() . 'structure/' . $template_name;
			break;
		} else {
			echo 'File '.RH_Help::template_dir() . 'structure/' . $template_name.' not found<br>';
		}
	}
 
	if ( ( true == $load ) && ! empty( $located ) )
		load_template( $located, $require_once );
 
	return $located;
}

function rh_page( $page = null, $show = false ) {
	$var = RH_Rewrite::$slug;
	#echo '<pre>', print_r( $var , 1), '</pre>';
	foreach ($var as $slug => $name) {
		if ( get_query_var( $slug ) === $name && $show ) return true;
		if ( get_query_var( $slug ) && !$page ) return $name;
		if ( get_query_var( $slug ) && $page === $name ) return true;
		if ( get_query_var( $slug ) && $page !== $name) return false;
	}
	//if( get_query_var( 'type' ) === 'readers_hub' ) return true;
	if( !$page && get_query_var('rh') ) return get_query_var('rh');
	if( get_query_var('rh') && $page === 'reading-view' ) return true ;

	if( !$page && get_query_var('rh_username') ) return 'reader-profile';
	if( get_query_var('rh_username') && $page === 'reader-profile' ) return true;

	if( !$page && get_query_var('mem_list') ) return 'members-list';
	if( get_query_var('mem_list') && $page === 'members-list' ) return true;

	if( !$page && get_query_var('rr_list') ) return 'reading-list';
	if( get_query_var('rr_list') && $page === 'reading-list' ) return true;

	return false;
}

function rh_page_active(&$rh_page) {
	$rh_page = true;
	return $rh_page;
}

function rh_page_checker() {
	global $rh_page;
	rh_page_active( $rh_page );
}
add_action('rh/head', 'rh_page_checker');
add_action('rh/before/content', 'rh_page_checker');

function rh_user( $key, $data = 'data' ) {
	$user = wp_get_current_user();
	if ( !$user || $user->ID == 0 ) return false;
	if ( !isset( $user->$data ) ) return 'Data Don\'t Exist';
	return (  is_object( $user->$data ) ) ? $user->$data->$key : $user->$data ;
}

function get_reading( $default = array() ) {
	$arg = RH_Reading::args_modify( array(
		'post_type'         => 'readers_hub',
		'posts_per_page'    => -1,
		'orderby'           => 'date',
		'order'             => 'DESC',
		'meta_key'			=> '_rh_assign_to',
		'meta_value'		=> rh_user('ID'),
	), $default );
	$query = new WP_QUery( $arg ); 
	$readings = $query->get_posts();
	return $readings;
}

function rh_loop( $object, $result, $before = '', $after = '' ) {
	$out = $before;
	foreach ( $object as $key => $value ) {
		$out .= $value->$result;
	}
	$out .= $after;
	return $out;
}

function rh_role( $role = null, $id = null ) {
	$user = ( $id ) ? get_userdata( (int) $id ) : wp_get_current_user();
	if ( !$user ) return false;
	$roles = apply_filters('rh/roles', array(
		'reader' 		=> array('ftr_reader','administrator', 'bbp_participant'),
		'poster' 		=> array('ftr_user'),
		'canbe_reader'	=> array('none'),
		'admin'  		=> 'administrator',
	));
	if ( !array_key_exists($role, $roles) ) return false;
	if ( in_array($roles[$role], $user->roles) ) return true;
	if ( is_array( $roles[$role]) ) {
		foreach ($roles[$role] as $key => $value) {
			if ( in_array($value, $user->roles) ) return true;
		}
	}
	return false;
}

function user_is_rh() {
	$user = wp_get_current_user();
	$roles = array('ftr_reader', 'ftr_user', 'bbp_participant');
	foreach ($roles as $role => $val ) {
		if ( in_array($val, $user->roles ) && !in_array('administrator', $user->roles) )
			return true;
	}
}

function reading_is_close( $id ) {
	$status = get_post_meta( $id, '_rh_status', true );
	if ( $status === 'closed' ) return true;
	return false;
}

function rh_next_r(&$array, $curr_val) {
	if ( !is_array( $array ) ) return false;
    $next = 0;
    reset($array);
    do {
        $tmp_val = current($array);
        $res = next($array);
    } while ( ($tmp_val != $curr_val) && $res );
    if( $res ) {
        $next = current($array);
    }
    return $next;
}

function rh_prev_r(&$array, $curr_val) {
	if ( !is_array( $array ) ) return false;
    end($array);
    $prev = current($array);
    do {
        $tmp_val = current($array);
        $res = prev($array);
    } while ( ($tmp_val != $curr_val) && $res );
    if( $res ) {
        $prev = current($array);
    }
    return $prev;
}

function rh_reader_data( $data = null, $id=null, $use_current=true ) {
	$user = ( $id ) ? $id : rh_user('ID');
	$udata = ( $use_current ) ? get_userdata($user) :  get_userdata($id);
	if ( !$udata ) return false;
	$uname = get_user_meta( $udata->ID, '_rh_display_uname', true );
	$uava  =  get_user_meta( $udata->ID, '_rh_display_uava', true );
	$umail = get_user_meta( $udata->ID, '_rh_user_email', true );
	$name = ( $uname ) ? $uname : $udata->display_name;
	$image = ( $uava ) ? $uava : get_avatar_url( $udata->ID );
	$mail = ( $umail ) ? $umail: $udata->user_email;
	$reader = array(
		'name'		=> $name,
		'image'		=> $image,
		'imager'	=> '<img class="user-img" src="'.$image.'" alt="'.$name.'" />',
		'mail'		=> $mail,
		'timez'		=> get_user_meta( $udata->ID, '_rh_user_timez', true ),
		'weekly'	=> get_user_meta( $udata->ID, '_rh_user_weekly', true ),
		'seldate'	=> get_user_meta( $udata->ID, '_rh_user_seldate', true ),
		'onleave'	=> get_user_meta( $udata->ID, '_rh_user_onleave', true ),
		'location'	=> get_user_meta( $udata->ID, '_rh_user_location', true ),
		'gender'	=> get_user_meta( $udata->ID, '_rh_user_gender', true ),
		'birthday'	=> get_user_meta( $udata->ID, '_rh_user_birthday', true ),
		'bio'		=> wpautop( get_user_meta( $udata->ID, 'description', true) ),
	);
	if ( !$data ) return $reader;
	return $reader[ $data ];
}

function checking_list_user_order( $i = 0 ) {
	
	
	$users = RH_Reading::list_users();
	$list = array();
	if ( !$users ) return false;
	foreach ($users as $user => $name ) {
		$list[]	= $user;
	}
	array_push( $list, array_shift($list) );
	echo '<pre>', print_r( $list, 1), '</pre>';
}

function rh_reader_select( $random = false, $id=null, $active='' ) {
	$users = RH_Reading::list_users( true );
	if ( !$users ) return false;
	if ( $random ) {
		if ( $id ) unset($users[$id]);
		return array_rand($users);
	}
	$select = '<select name="rh_reader" id="reader-list" class="user-select">';
	foreach ( $users as $user => $data ) {
		$reading = RH_Reading::counter('closed', '', '', $user, true);
		#$count = sprintf( _n( '%s Reading', '%s Readings', $reading, 'trh' ), $reading );
		$count = ( $reading > 1 ) ? $reading . ' Readings': $reading. ' Reading' ;
		$selected = ( $user == $active ) ? ' selected' : '' ;
		$select .= '<option data-average="'.RH_Reading::average( $user ).'" data-total="'.$count.'" data-img="'.rh_reader_data('image', $user, false).'" value="'.$user.'"'.$selected.'>'. rh_reader_data('imager', $user, false).$data .'</option>';
	}
	return $select.'</select>';
}

function rh_reader_available( $id ) {
	$suspend = get_user_meta($id, '_rh_user_suspend', true );
	if ( $suspend == 1 ) return false;
	$comp_date = new DateTime( current_time('mysql') );
	$usertimez = get_user_meta($id, '_rh_user_timez', true);
	if ( $usertimez  ) {
		$comp_date->setTimezone(new DateTimeZone( $usertimez ));
	}
	$cur = $comp_date->format('m/d/Y');
	$day = $comp_date->format('l');
	$onleave = rh_reader_data( 'onleave', $id, false );
	$dates   = rh_reader_data( 'seldate', $id, false );
	$weekly   = rh_reader_data( 'weekly', $id, false );
	if ( !$id || $onleave ) return false;
	if ( is_array( $dates ) && isset( $dates[$cur] ) ) {
		if ( $dates[$cur] > rh_assign_today( $id ) ) return true;
	} 
	if ( is_array( $weekly ) ) {
		if ( isset( $weekly[$day] ) && $weekly[$day] > rh_assign_today( $id ) ) return true;
	}
}

function rh_assign_today( $id ) {
	$comp_date = new DateTime( current_time('mysql') );
	$usertimez = get_user_meta($id, '_rh_user_timez', true);
	if ( $usertimez  ) {
		$comp_date->setTimezone(new DateTimeZone( $usertimez ));
	}
	$type = array(
		'meta_key'			=> '_rh_assign_to',
		'meta_value'		=> $id,
		'year'				=> $comp_date->format('Y'),
		'monthnum'			=> $comp_date->format('n'),
		'day'				=> $comp_date->format('j'),
	);
	$readings = get_reading( $type );
	return count( $readings );
}

function rh_customer_can_post( $id ) {
	$arg = array(
		'meta_key'          => '_rh_assign_to',
		'meta_value'        => '',
		'author'            =>  $id,
		'orderby'           =>  'post_date',
		'order'             =>  'DESC',
		'posts_per_page'    => -1,
		'year'				=> date('Y'),
		'monthnum'			=> date('m'),
	);

	$request = get_reading( $arg );
	$limit = get_user_meta( $id, '_rh_user_monlimit', true );

	if ( count( $request ) < $limit ) return true;
	return false; 
	//return count( $request );
	/*$request = get_reading( $arg );
	if( !$request ) return 0;
	$now = new DateTime( current_time('mysql') );
	$posted =  new DateTime( $request[0]->post_date );
	$posted->modify('30 day');
	$t = $posted->diff( $now );
	return $t->days;*/
}

function rh_user_stat( $id ) {
	$user = get_userdata(  $id );
	$type = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
	            'key' => '_rh_status',
				'value' => array('closed','awiting_feedback'),
				'compare' => 'IN',
	        ),
	        array(
	            'key' => '_rh_assign_to',
				'value' => $id,
				'compare' => '=',
	        )
		),
	);
	if ( !rh_role('reader', $id ) ) return false;
	$status = ( in_array('administrator', $user->roles) ) ? 'Admin | ' : '';
	$readings = get_reading( $type );
	if ( !$readings || count($readings) < 11 ) return $status .'Tarot Explorer';
	if ( count($readings) < 31 ) return $status .'Tarot Reader';
	if ( count($readings) < 101 ) return $status .'Tarot Maven';
	if ( count($readings) > 100 ) return $status .'Tarot Sage';
}

function rh_poster_data( $id, $key=false ) {
	$poster = get_post_field( 'post_author', $id );
	if ( !$poster ) return false;
	$poster_data = get_userdata( $poster );
	return $poster_data;
}
function rh_assigned_reader( $id ) {
	$reader = get_post_meta( $id, '_rh_assign_to', true );
	if ( !$reader ) return false;
	$reader_data = get_userdata( $reader );
	return $reader_data;
}
function rh_gender_select( $active=null, $class='') {
	$opt = array('Male','Female');
	$select = '<select id="user-gender" name="user_gender" class="'.$class.'">';
	foreach ($opt as $key => $value) {
		$active = ( $value == $active ) ? ' selected': '' ;
		$select .= '<option value="'.$value.'"'.$active.'>'.$value.'</option>';
	}
	return $select.'</select>';
}

function _rh_user_banned( $id ) {
	$banned = get_user_meta($id, '_rh_user_ban', true);
	if ( $banned ) return true;
	return false;
}

function _rh_current_ip() {
	$ip = '';
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function _rh_ip_banned() {
	$ban = get_option('rh_bannedIP');
	$bannedIP = $ban && isset( $ban['_rh_banned_ips'] ) ? explode("\n", $ban['_rh_banned_ips'] ) : false ;
	if ( !$bannedIP ) return false;
	if ( in_array( _rh_current_ip(), $bannedIP ) ) return  true;
	return false;
}

function _rh_get_star_rating( $id, $default = false ) {
	$feedback = RH_Reading::get_message($id, 'rh_feedback');
	$rating = ( isset( $feedback[0] ) ) ? (int) get_comment_meta( $feedback[0]->comment_ID, 'rh_rating', true ) : 0;
	if ( $default ) return $rating;
	$overate = get_post_meta( $id, '_rh_request_rating', true);
	return ( $overate ) ? $overate : $rating;
}