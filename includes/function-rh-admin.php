<?php
/**
 * Package Included for Readers Hub Plugin
 * 
 * Do not Modify this under any circumtances
 *
 * @package     Readers Hub 
 * @subpackage  Readers Hub Admin Functions includes post types, metas, taxonomies ec, 
 * @author      SilverKenn
 * @version     1.0
 */
if( !defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'CMB2_Bootstrap_209', false ) ) {
	require_once 'meta/init.php';
}

function _read_hub_post_type() { 
	register_post_type( 'readers_hub', 
		array('labels' => array(
			'name' => __('Readers Hub', 'read-hub'), 
			'singular_name' => __('Reading', 'read-hub'), 
			'all_items' => __('All Readings', 'read-hub'),
			'add_new' => __('Add New', 'read-hub'),
			'add_new_item' => __('Add Reading', 'read-hub'),
			'edit' => __( 'Edit'), 
			'edit_item' => __('Edit Reading', 'read-hub'), 
			'new_item' => __('New Reading', 'read-hub'), 
			'view_item' => __('View Reading', 'read-hub'), 
			'search_items' => __('Search Reading', 'read-hub'), 
			'not_found' =>  __('Nothing found in the Database.', 'read-hub'), 
			'not_found_in_trash' => __('Nothing found in Trash', 'read-hub'), 
				'parent_item_colon' => ''
			), 
			'description' => __( 'Add Reading In this section', 'read-hub'),
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, 
			'menu_icon' => 'dashicons-media-document',
			'rewrite'	=> array( 'slug' => 'tarrot-reading', 'with_front' => false ),
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor' ),
	 	) 
	);
} 
add_action( 'init', '_read_hub_post_type');


function _remove_readers_hub_autosave() {
    if ( 'readers_hub' == get_post_type() )
        wp_dequeue_script( 'autosave' );
}
add_action( 'admin_enqueue_scripts', '_remove_readers_hub_autosave' );

function _readers_hub_meta_opt() {
	$p = '_rh_';
	
	// For Admin Options Readers Hub Post type
	$rh = new_cmb2_box( array(
		'id'			=> $p . 'data',
		'title'			=> __( 'Reading Option', 'trh' ),
		'object_types'	=> array( 'readers_hub' ), 
		'show_on_cb'	=> '_rh_option_for_admin',
		'context'		=> 'normal',
		'priority'		=> 'high',
		//'show_names'	=> false,
	));
    /*$rh->add_field( array(
		'name'             => __( 'Assign to User', 'trh' ),
		'id'               => $p . 'assign_to',
		'type'             => 'select',
		'show_option_none' => true,
		//'default'          => RH_Reading::read_meta('assign_to'),
		'options'          => RH_Reading::list_users( true ),
	));*/
	$rh->add_field( array(
		'name'		=> __( 'Change Reading Status', 'trh' ),
		'id'		=> $p . 'status',
		'type'		=> 'radio_inline',
		//'default'	=> RH_Reading::read_meta('status'),
		'options'	=> RH_Reading::status( true ),
	));

	$cm = new_cmb2_box( array(
		'id'			=> $p . 'comments',
		'title'			=> __( 'Conversation', 'trh' ),
		'object_types'	=> array( 'readers_hub' ), 
		'show_on_cb'	=> '_rh_option_for_admin',
		'context'		=> 'normal',
		'priority'		=> 'high',
		//'show_names'	=> false,
	));
	$cm->add_field( array(
	    'type' => 'title',
	    'id'   => 'comments_title',
	    'before_field' => '_rh_reading_messages',
	));

	$feedb_over = new_cmb2_box( array(
		'id'			=> $p . 'feedback',
		'title'			=> __( 'Override Rating', 'trh' ),
		'object_types'	=> array( 'readers_hub' ), 
		'show_on_cb'	=> '_rh_option_for_archive',
		'context'		=> 'normal',
		'priority'		=> 'high',
		'show_names'	=> false,
	));
	$feedb_over->add_field( array(
	    'name'      => __( 'Star Rating', 'cmk' ),
		'id'        => '_rh_request_rating',
		'type' 		=> 'text_small',
		'default'	=> '_rh_default_rating',
		'attributes' => array(
			'type' => 'number',
			'pattern' => '\d*', 
			'min'	=> '0',
		),
	));

	// For Admin Options Edit User Page

	$cust = new_cmb2_box( array(
		'id'                => $p . 'customer',
		'title'             => __( 'Customer Data', 'cmk' ),
		'object_types'      => array( 'user'),
		'show_on_cb'        => '_rh_option_for_admin',
		'show_names'        => true,
		'new_user_section'  => 'add-new-user',
	));
	$cust->add_field( array(
		'name'      => __( 'RH Name', 'cmk' ),
		'id'        => '_rh_display_uname',
	    'type'    => 'text',
	));
	$cust->add_field( array(
		'name'      => __( 'RH Email', 'cmk' ),
		'id'        => '_rh_user_email',
	    'type'    => 'text',
	));

	$cust->add_field( array(
		'name'      => __( 'RH Display Image', 'cmk' ),
		'id'        => '_rh_display_uava',
		'type' => 'file',
	    'options' => array(
	        'url' => false, 
	    ),
	));
	$cust->add_field( array(
		'name'      => __( 'RH Gender', 'cmk' ),
		'id'        => '_rh_user_gender',
		'type'             => 'select',
	    'options'          => array(
	        'Male' => __( 'Male', 'cmk' ),
	        'Female'   => __( 'Female', 'cmk' ),
	    ),
	));
	$cust->add_field( array(
		'name'      => __( 'RH Location', 'cmk' ),
		'id'        => '_rh_user_location',
		'type'		=> 'select',
	    'options'	=> RH_Help::country('', '', true),
	));
	
	$cust->add_field( array(
		'name'      => __( 'RH Birthday', 'cmk' ),
		'id'        => '_rh_user_birthday',
		'type'    => 'text',
	));

	$cust->add_field( array(
		'name'      => __( 'RH Request Limit Per Month', 'cmk' ),
		'id'        => '_rh_user_monlimit',
		'type' 		=> 'text_small',
		'default'	=> 1,
		'attributes' => array(
			'type' => 'number',
			'pattern' => '\d*', 
			'min'	=> '0',
		),
	));
	$cust->add_field( array(
	    'name'             => 'RH Ban User',
	    'id'               => '_rh_user_ban',
	    'type'             => 'select',
	    'default'          => false,
	    'options'          => array(
	        false =>	'No',
	        true  =>	'Yes',
	    ),
	) );

	$reader = new_cmb2_box( array(
		'id'                => $p . 'reader',
		'title'             => __( 'Reader Data', 'cmk' ),
		'object_types'      => array( 'user'),
		'show_on_cb'        => '_rh_option_for_reader',
		'show_names'        => true,
		'new_user_section'  => 'add-new-user',
	));
	
	$reader->add_field( array(
		'name'      => __( 'RH OnLeave', 'cmk' ),
		'id'        => '_rh_user_onleave',
		'type'    => 'radio_inline',
	    'options' => array(
	        1 => __( 'Yes', 'cmb2' ),
	        0 => __( 'No', 'cmb2' ),
	    ),
	));
	
	$reader->add_field( array(
		'name'      => __( 'RH Timezone', 'cmk' ),
		'id'        => '_rh_user_timez',
	    'type'    => 'text',
	));
}
add_action( 'cmb2_admin_init', '_readers_hub_meta_opt' );

function _rh_reading_messages() {
	echo '<style>.pdt,.pdt.rview{color:#B3B3B3}.reading-msg li{display:block;position:relative;border:1px solid #EEE;padding:10px;overflow:hidden;margin-bottom:10px}.reading-msg .avatar,.reading-msg .user-img{max-width:20px;max-height:20px;border-radius:50px}.pdt{font-size:12px;font-style:normal}.reading-msg .ftr-user{color:#A67FB5;font-weight:400;font-size:12px}.msg-cont{width:90%;float:right}.pdt.rview,.user-img{float:left;margin-right:18px;}.msg-cont p{margin-top:0}</style><ul class="reading-msg">';
	$messages = RH_Reading::get_message( get_the_ID() );
	foreach ($messages as $message => $obj) {
		$user = ( rh_role( 'reader', $obj->user_id ) ) ? 'ftr-user' : 'normal-user' ;
		echo '<li class="responses '.$user.'"><span class="user-img">'.rh_reader_data('imager', $obj->user_id ) .'</span>' .'<i class="pdt rview">'.date('M d', strtotime( $obj->comment_date ) ). '</i>'. 
			'<div class="msg-cont">'. wpautop( $obj->comment_content ).'</div></li>';
	}
	echo '</ul>';
}

function _rh_option_for_admin() {
    $user = wp_get_current_user();
    if ( !rh_role('admin') ) {
        return false;
    }
    return true;
}

function _rh_option_for_reader() {
    global $user_id;
    if ( !rh_role('reader', $user_id ) ) {
        return false;
    }
    return true;
}

function _rh_option_for_archive( $id ) {
    $status = get_post_meta( $id->object_id(), '_rh_status', 1 );
    return 'closed' === $status;
}

function _rh_default_rating( $field_args, $field ) {
    return _rh_get_star_rating(  $field->object_id, true );
}

add_filter('manage_edit-readers_hub_columns', '_read_hub_assinged_to_col', 1, 2 );
add_action( 'manage_readers_hub_posts_custom_column', '_read_hub_assinged_to_col_val', 1, 2 );

function _read_hub_assinged_to_col( $col ) {
	unset($col['date']);
	$col['posted_by'] =__( 'Posted By','brgm' );
    $col['assign_to'] =__( 'Assigned To','brgm' );
    $col['status'] =__( 'Reading Status','brgm' );
    $col['date'] =__( 'Date','brgm' );
    unset($col['comments']);
    return $col;
}

function _read_hub_assinged_to_col_val( $col, $id ) {
    if ( 'assign_to' === $col ) {
    	$data = rh_reader_data('name', get_post_meta($id, '_rh_assign_to', true), false );
    	if( $data ) echo $data;
    } else if ( 'status' === $col ) {
    	$data = RH_Reading::status( get_post_meta($id, '_rh_status', true), false );
    	if( $data ) echo $data;
    }
    else if ( 'posted_by' === $col ) {
    	$data = get_userdata( get_post_field ('post_author', $id) );
    	if( $data ) echo $data->display_name;
    }
   
}

#add_action( 'save_post', '_update_reading_status', 999, 2 );
function _update_reading_status( $id, $post ) {
	if ( 'readers_hub' != $post->post_type ) {
        return;
    }
    if ( isset( $_REQUEST['_rh_assign_to'] ) && $_REQUEST['_rh_assign_to'] == '' ) {
        update_post_meta(  $id, '_rh_status', 'open' );
		update_post_meta(  $id, '_rh_assign_date', '');
    } else if ( isset( $_REQUEST['_rh_assign_to'] ) && $_REQUEST['_rh_status'] === 'open' ) {
    	update_post_meta(  $id, '_rh_status', 'assigned' );
    }
    if ( isset( $_REQUEST['_rh_assign_to'] ) && $_REQUEST['_rh_assign_to'] !== '' ) {
    	update_post_meta(  $id, '_rh_assign_date', current_time( 'mysql' ) );
    }
}

add_action('admin_menu', '_rh_admin_option' );
add_action('cmb2_admin_init', '_rh_admin_meta');

function _rh_admin_option() {
	add_submenu_page(
        'edit.php?post_type=readers_hub',
        'Start Content',
        'Start Content',
        'manage_options',
        'rh_start',
        '_rh_create_options_meta' 
	);

	add_submenu_page(
        'edit.php?post_type=readers_hub',
        'Banned IP',
        'Banned IP',
        'manage_options',
        'rh_bannedIP',
        '_rh_create_banned_option' 
	);
}

function _rh_create_options_meta() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap start-settings"><h3>Start Page Content</h3>';
		cmb2_metabox_form( '_rh_start_settings', 'rh_start' );
	echo '</div>';
}

function _rh_create_banned_option() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap start-settings"><h3>Banned IP</h3><span>One IP per Line</span>';
		cmb2_metabox_form( '_rh_banned_opt', 'rh_bannedIP' );
	echo '</div>';
}

function _rh_admin_meta() {
	$jrp = new_cmb2_box( array(
		'id'      => '_rh_start_settings',
		'title'   => __( 'Start Page', 'brgm' ),
		'hookup'  => false,
		'show_on' => array(
			'key'   => 'options-page',
			'value' => array( 'rh_start' )
		),
	));
	$jrp->add_field( array(
		'id'   => '_start_content',
		'type' => 'wysiwyg',
	)); 

	$jrp = new_cmb2_box( array(
		'id'      => '_rh_banned_opt',
		'title'   => __( 'Banned IP', 'brgm' ),
		'hookup'  => false,
		'show_on' => array(
			'key'   => 'options-page',
			'value' => array( 'rh_bannedIP' )
		),
	));
	$jrp->add_field( array(
		'id'   => '_rh_banned_ips',
		'type' => 'textarea_code',
	)); 
}

function _rh_shorcode() {
	$sc = array(
		'rh_url' => RH_URL,
		'rh_dir' => RH_DIR,
	);
	return $sc;
}