<?php
/*
 * Plugin Name: Readers Hub
 * Version: 1.7.0
 * Plugin URI: http://localhost/
 * Description: Plugin for creating custom site functionality
 * Author: SilverKenn
 * Author URI: http://www.symmetricsweb.com
 * Requires at least: 4.0
 * Tested up to: 4.2.4
 *
 * Text Domain: read-hub
 * Domain Path: /trh/
 *
 * @package Readers Hub
 * @author SilverKenn
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define('RH_DIR', plugin_dir_path( __FILE__ ) );
define('RH_URL', plugin_dir_url( __FILE__ ) );

require_once( 'includes/function-rh-admin.php' );
require_once( 'includes/class-rh-help.php' );
require_once( 'includes/class-rh-rewrite.php' );
require_once( 'includes/function-rh-helper.php' );
require_once( 'includes/function-rh-build.php' );
require_once( 'includes/class-rh-readings.php' );


$rh_page = false;

register_activation_hook( __FILE__, '_rh_activate' );
function _rh_activate() {
	RH_Rewrite::activate();
	RH_Reading::roles();
}

add_action('init', '_rh_init');
function _rh_init() {
	$codes = _rh_shorcode();
	foreach ($codes as $name => $output ) {
		add_shortcode( $name, function() use ( $output ){ return $output; });
	}
}
