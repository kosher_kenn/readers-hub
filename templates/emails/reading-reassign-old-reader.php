<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {reader_name},
<br><br>
Your reading for {poster_name} has been reassigned to a new reader. You will no longer see this request in your Reader Dashboard and you do not need to respond the client.
<br><br>
Please keep in mind that if we need to reassign your readings on multiple occasions, you may be asked to leave the Free Tarot Readings service. 
<br><br>
Of course, we don't want that to happen so please make sure your availability is up-to-date and you keep track of your allocated readings via the Free Tarot  <a href="{reading_url}">Readings dashboard.</a>
<br><br>
If you have any questions or concerns, hit Reply and we will be in touch.
<br><br>
Thanks,<br>
Team Biddy
