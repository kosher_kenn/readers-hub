<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {poster_name},
<br><br>
You have a new message from your Biddy Tarot reader, {reader_name}. 
<br><br>
<a href="{reading_url}">Click here</a> to view and respond to the message.
<br><br>
Thanks,<br>
Team Biddy
