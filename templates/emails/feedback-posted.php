<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {reader_name},
<br><br>
Your Biddy Tarot client, {poster_name}, left you feedback on your recent reading. 
<br><br>
<a href="{reading_url}">Click here to read the feedback</a>. 
<br><br>
Thanks,<br>
Team Biddy