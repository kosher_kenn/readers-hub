<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {reader_name},
<br><br>
You have a new request for a Tarot reading:
<br><br>
Name: {poster_name}
<br><br>
Due Date: {due_date}
<br><br>
Question: {question}
<br><br>
Please log in to the <a href="{reading_url}">Free Tarot Readings dashboard</a> to view the full details and respond to your client.
<br><br>
Note: This reading has been reassigned from a previous reader who was not able to fulfil their commitment. While you have 7 days to complete the reading, it would be much appreciated if you could complete it in a shorter period of time. 
<br><br>
If you have any questions, please contact the team at community@biddytarot.com
<br><br>
Thanks,<br>
Team Biddy
