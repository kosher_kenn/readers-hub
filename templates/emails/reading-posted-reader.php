<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {reader_name},
<br><br>
You have a new request for a Tarot reading:
<br><br>
Name: {poster_name}
<br><br>
Due Date: {due_date}
<br><br>
Question: {question}
<br><br>
Please log in to the Free Tarot Readings dashboard to view the full details and respond to your client.
<br><br>
If you have any questions, please contact our team at community@biddytarot.com
<br><br>
Thanks,<br>
Team Biddy
