<?php
if( !defined( 'ABSPATH' ) ) exit;
?>
Hi {poster_name},
<br><br>
Thanks for requesting a free Tarot reading at Biddy Tarot.
<br><br>
Your request has been allocated to {reader_name} who will provide your Tarot reading within 7 days.
<br><br>
If you need to contact your reader, <a href="{reading_url}">please do so here</a>:
<br><br>
Login Details
<br>
Username: {login}
<br>
Password: {password}
<br><br>
If you have any questions, please contact our team at community@biddytarot.com
<br><br>
Thanks,
<br>
Team Biddy

