<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {poster_name},
<br><br>
Your reading as been reassigned to {reader_name}. Please note that it may take a further 7 days until you receive your free reading.
<br><br>
If you need to contact your reader, <a href="{reading_url}">please do so here:</a>
<br><br>
If you have any questions, please contact our team at community@biddytarot.com
<br><br>
Thanks,<br>
Team Biddy
