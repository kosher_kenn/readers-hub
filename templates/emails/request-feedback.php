<?php
if( !defined( 'ABSPATH' ) ) exit;
?>

Hi {poster_name},
<br><br>
We hope you've found your free Tarot reading helpful.
<br><br>
Your reader, {reader_name}, has asked you to provide feedback about the Tarot reading.
<br><br>
<a href="{reading_url}">Click here</a> to share your feedback.
<br><br>
It will only take 2 minutes and it will help {reader_name} continue to grow as a Tarot reader. (Plus, it's part of the deal!)
<br><br>
Thanks,<br>
Team Biddy