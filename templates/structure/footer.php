		<?php do_action('rh/before/footer'); ?>
		<div class="site-footer">
			<?php do_action('rh/footer'); ?>
		</div>
		<?php do_action('rh/after/footer'); ?>

	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="<?php echo RH_URL ?>assets/js/mutlidate.js"></script>
	<script src="<?php echo RH_URL ?>assets/js/rh-script.js"></script>
	<?php do_action('rh/after/container'); ?>	
</body>
</html>