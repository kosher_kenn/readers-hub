<?php global $wp_rewrite; 
$time = time();
if ( isset( $_GET['rhuser'] ) && $_GET['rhuser'] == 'check' ) echo '<pre>', print_r( wp_get_current_user(), 1), '</pre>';
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<title>Readers Hub | <?php do_action('rh_page_title'); ?></title>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex, nofollow" />
	<!--[if lte IE 8]><script src="<?php echo RH_URL; ?>assets/js/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat|Roboto+Slab" type="text/css" media="all" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/css/style.css?no=<?php echo $time ?>" type="text/css" media="all" />	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo RH_URL ?>assets/js/rh-framework.js"></script>
	<!--[if lte IE 9]><link rel="stylesheet" href="<?php echo RH_URL; ?>assets/css/ie9.css" /><![endif]-->
	<!--[if lte IE 8]><link rel="stylesheet" href="<?php echo RH_URL; ?>assets/css/ie8.css" /><![endif]-->
<?php do_action('rh/head'); ?></head>
<body <?php body_class(); ?>><?php do_action('rh/before/container'); ?>
	<div class="site-container"><?php do_action('rh/before/header'); ?>
		<div class="site-header">
			<div class="wrap"><?php do_action('rh/header'); ?>
			</div>
		</div><?php do_action('rh/after/header'); ?>