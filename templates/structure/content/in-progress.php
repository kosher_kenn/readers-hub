<?php if( !rh_role('reader') ) die('You aren\'t allowed to be here!');	
$editor_id = 'new_rh_reading';
global $wp_query;
$settings =   array(
    'wpautop' => true,
    'media_buttons' => true, 
    'textarea_name' => $editor_id, 
    'textarea_rows' => get_option('default_post_edit_rows', 5), 
    'tabindex' => '',
    'editor_css' => '', 
    'editor_class' => '', 
    'teeny' => false, 
    'dfw' => false, 
    'tinymce' => true, 
    'quicktags' => true 
); 
$type = array(
	'meta_query' => array(
		'key' => '_rh_status',
		'value' => 'inprogress',
		'compare' => '=',
	),
);
$readings = get_reading( $type );
?>
<div class="reading-content reading-section clear">
	<?php if ( !$readings ) {
		echo '<h3 align="center" style="padding-top: 50px">You don\'t have any in-progress readings</h3></div>';
		return false;
	} ?>
	<div class="left-reading">
	<ul class="user-reading-list">
	<?php 
	foreach ($readings as $reading => $key ) {
		$user = get_userdata( $key->post_author );
		$active = ( $key === reset( $readings )  ) ? ' active' : '' ;
		echo '<li class="reading-item trans'. $active.'" data-id="'.$key->ID.'"><i class="pdt">'.
			date('M d', strtotime( $key->post_date ) ). '</i>';
		if ( $user ) {
			echo rh_reader_data('imager', $user->ID, false).'<span class="name">'.
				rh_reader_data('name', $user->ID, false).
				'<i class="loc">'.rh_reader_data('location', $user->ID, false).'</i><i class="gender">'.
				rh_reader_data('gender', $user->ID, false).'</i><i class="bday">Born in '.
				rh_reader_data('birthday', $user->ID, false).'</i></span>';
		} else {
			echo '<span class="name">User No Longer Exist</span>';
		}
		echo '<span class="due-date">Due in '.RH_Reading::due_date( $key->ID ).'</span></li>';
	}
	?>
	</ul>
	</div>
	<div class="right-reading">
	<?php 
	$get_draft = RH_Reading::get_message( $readings['0']->ID, 'rh_draft' );
	$draft = ( $get_draft ) ? $get_draft['0']->comment_content : '' ;
	foreach ($readings as $reading => $key ) {
		$messages = RH_Reading::get_message( $key->ID );
		$active = ( $key === reset( $readings )  ) ? ' active' : '' ;
		echo '<div class="reading-msg trans'.$active.'" id="reading-'.$key->ID.'"><ul><li><span class="user-img"><a class="fancybox" href="'.rh_reader_data('image', $key->post_author, false).'">'.
			rh_reader_data('imager', $key->post_author, false) .'</a></span><div class="msg-cont">'. wpautop( $key->post_content ).'</div></li>';
			if ( $messages ) {
				foreach ($messages as $message => $obj) {
					$user = ( rh_role('reader', $obj->user_id ) ) ? 'ftr-user' : 'normal-user' ;
					echo '<li class="responses '.$user.'"><span class="user-img"><a class="fancybox" href="'.rh_reader_data('image', $key->post_author, false).'">'.
			rh_reader_data('imager', $key->post_author, false) .'</a></span><div class="msg-cont">'. 
					wpautop( $obj->comment_content ).'</div></li>';
				}
			}
		echo '</ul></div>';
		
	}
	wp_editor( $draft, 'new_rh_reading', $settings );
	echo '<input type="hidden" id="editing-id" value="'. $readings['0']->ID .'" />';
	echo '<button class="btn send-reading-response follow" data-id="send-followup">Send Message </button>';
	echo '<button class="btn send-reading-response reading" data-id="send-reading">Send Reading</button>';
	?>
	</div>
</div>
<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/js/fancybox/source/jquery.fancybox.css" type="text/css" media="all" />
<script src="<?php echo RH_URL ?>assets/js/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
(function($) {
	$(document).ready( function() {
		$(".fancybox").fancybox({'padding': 5});
	});
})(jQuery);
</script>