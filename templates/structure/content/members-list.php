<?php 
if( !rh_role('admin') ) die('You aren\'t allowed to be here!');

$page = ( get_query_var('mem_list') ) ? get_query_var('mem_list') : 1;

$role = array('ftr_reader','bbp_participant');

$search = isset( $_GET['search'] ) ? $_GET['search'] : false;

$args = array(
	'role__in'	=> $role,
	'number' 	=> 20, 
	'orderby'	=> 'ID',
	'paged' 	=> $page, 
);
$all_args = array(
	'role__in'	=> $role,
	'number' 	=> 9999, 
);

$sq = array(
	'relation' => 'OR',
	array(
		'key'     => '_rh_display_uname',
		'value'   => $search,
		'compare' => 'LIKE'
	),
	array(
		'key'     => 'first_name',
		'value'   => $search,
		'compare' => 'LIKE'
	),
	array(
		'key'     => 'last_name',
		'value'   => $search,
		'compare' => 'LIKE'
	),
);

if ( $search ) {
	//$args['search'] = "*{$search}*";
	//$args['search_columns'] = array( 'ID');
	$args['meta_query'] = $sq;
	$all_args['meta_query'] = $sq;
}

$uquery = new WP_User_Query( $args );
$users = $uquery->results;

$all_reader = new WP_User_Query( $all_args );
$total = ceil( count( $all_reader->results ) / $args['number'] );

$page_args = array(
	'base'               => RH_Rewrite::url('members-list') . '%_%',
	'format'             => '/%#%',
	'total'              => $total,
	'current'            => $page,	
	'prev_text'          => __('«'),
	'next_text'          => __('»'),
);
//$readers = RH_Reading::list_users();
$user_info = '';
if ( $users ) {
	foreach ($users as $usr => $data ) {
		$id = $data->ID;
		$link = RH_Rewrite::url('index'). '/reader/'.$id;
		$onleave = get_user_meta($id, '_rh_user_suspend', true );
		$suspend = ( $onleave == 1 ) ? 'suspended' : 'suspend' ;
		$name = rh_reader_data('name', $id, false);
		$req = RH_Reading::counter('closed','','', $id, true );
		$unreq = RH_Reading::counter('open','','', $id, true );

		$user_info .= '<li id="user-'.$id.'" class="user-item" data-user="'. $id .'" data-name="'. $name .'" data-rating="'. RH_Reading::average($id) .'" data-req="'.$req.'" data-unreq="'. $unreq .'">';
		$user_info .= '<span class="ul-gb item">'.$id.'</span>';
		$user_info .= '<span class="ul-na item"><a href="'.$link.'"><i class="name">'.$name.'<u>'.rh_user_stat( $id ).'</u></i></a></span>';
		$user_info .= '<span class="ul-ar item">'.RH_Reading::average($id).'</span>';
		$user_info .= RH_Reading::counter('closed','<span class="ul-cr item">','</span>', $id, true );
		$user_info .= RH_Reading::counter('open','<span class="ul-ur item">','</span>', $id, true );
		$user_info .= '<span class="ul-su item"><i data-action="suspend" class="'.$suspend.' trans"></i></span>';
		//$data .= '<span class="ul-cm item"><i data-action="remove" class="cancel-mem trans"></i></span>';
		$user_info .= '</li>';
	}
}
?>
<div class="reading-content members-section">
	<form action="" method="GET" class="item-search">
		<input class="sbox" type="text" name="search" placeholder="Search Reader..." value="<?php echo $search; ?>" />
		<input type="Submit" value="Go" />
	</form>
	<ul class="users-lists list-format">
		<li class="list-heading">
			<span class="ul-gb item"><i class="sortit" data-order="user">Global<br>Position</i></span>
			<span class="ul-na item"><i class="sortit" data-order="name">Name</i></span>
			<span class="ul-ar item"><i class="sortit" data-order="rating">Average<br>Rating</i></span>
			<span class="ul-cr item"><i class="sortit" data-order="req">Completed<br>Requests</i></span>
			<span class="ul-ur item"><i class="sortit" data-order="unreq">Unanswered<br>Requests</i></span>
			<span class="ul-su item">Suspend<br>Reader</span>
			<!--span class="ul-cm item">Cancel<br>Membership</span-->
		</li>
		<?php echo $user_info; ?>
	</ul>
	<div class="res-nav"><?php echo paginate_links( $page_args ); ?></div>
	<div id="user-edit" class="user-editor" style="display:none;">
		<span class="edit-icon"></span>
		<h5 class="edit-note" align="center"></h5>
		<p class="edit-subnote" align="center"></p>
		<button class="btn close-edit">Cancel</button>
		<button id="user-update" data-action="" data-user="" class="btn close-edit btn-action">Delete</button>
	</div>
</div>
<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/js/fancybox/source/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo RH_URL ?>assets/js/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
var fancyboxjs = '<?php echo RH_URL ?>assets/js/fancybox/source/jquery.fancybox.pack.js';
(function($) {
	$(document).ready( function() {
	});
	$('body').on('click', '.suspend, .cancel-mem', function(e) {
		var user = $(this).closest('.user-item').data('user'),
			action = $(this).data('action'),
			ednote = ( action === 'suspend' ) ? 'Are you sure you want to suspend this reader?' : 'Are you sure you want to remove this user?';
			message = ( action === 'suspend' ) ? 'This reader will no longer receive reading request' : 'This user will be remove as Reader';
		$('#user-update').attr('data-action', action ).attr('data-user', user).text( action );
		$('.edit-note').html( ednote );
		$('.edit-subnote').html( message );
		$.fancybox({
			type 	: 'inline',
			content : $('#user-edit').clone(),
			afterClose : function() {
				$('#user-update').attr('data-action', '' ).attr('data-user', '').text( 'Delete' );
			},
			helpers : {
				overlay : {
			        css : { 'background' : 'rgba(0, 0, 0, 0.47)' }
			    }
			}
		});
	});

	$('body').on('click', '.suspended', function(e) {
		var user = $(this).closest('.user-item').data('user'),
			action = 'unsuspend',
			ednote = 'Are you sure you want to Unsuspend this reader?';
			message = 'This reader will receive reading request';
		$('#user-update').attr('data-action', action ).attr('data-user', user).text( 'Unsuspend' );
		$('.edit-note').html( ednote );
		$('.edit-subnote').html( message );
		$.fancybox({
			type 	: 'inline',
			content : $('#user-edit').clone(),
			afterClose : function() {
				$('#user-update').attr('data-action', '' ).attr('data-user', '').text( 'Delete' );
			},
			helpers : {
				overlay : {
			        css : { 'background' : 'rgba(0, 0, 0, 0.47)' }
			    }
			}
		});
	});

	$('body').on('click', '.close-edit', function() {
		$('.fancybox-close').trigger('click');
	});
})(jQuery);		
</script>