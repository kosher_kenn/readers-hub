<?php 
if( !rh_role('reader') ) die('You aren\'t allowed to be here!');
$start = get_option('rh_start'); ?>

<div class="start-content">
	<div class="section main-section">
		<?php if ( $start && isset($start['_start_content']) ) 
		echo do_shortcode( wpautop( $start['_start_content'] ) ); ?>
	</div>
</div>