<?php if( !rh_role('reader') ) die('You aren\'t allowed to be here!'); 
$average = ( RH_Reading::average() ) ? RH_Reading::average() : '0'; 
$ol = ( rh_reader_data('onleave') ) ? ' active' : '' ; 
$seldate = rh_reader_data('seldate');
$sd = '';
if ( is_array( $seldate ) ) {
	foreach ($seldate as $date => $count) {
		$sd .= '<p><input data-date="'.$date.'" type="number" step="1" min="0" max="10" value="'.$count.'" title="Qty"></p>';
	}
} ?>
<div class="dashboard-content">
	<div class="section section-1">
		<h4 class="section-heading">Your Stats:</h4>
		<div class="section-inner">
			<span class="user-avatar"><img src="<?php echo rh_reader_data('image'); ?>" alt="User"></span>
			<span class="reader-rate gold"><?php echo rh_user_stat( rh_user('ID') ) ?></span>
			<span class="reader-name"><?php echo rh_reader_data('name'); ?></span>
			<span class="stars">
				<i></i><i></i><i></i><i></i><i></i>
				<span class="ratings"><?php echo str_repeat('<i></i>', $average ); ?></span>
			</span>
			<div class="user-request">
				<span class="completed bottom-1"><span class="count gold"><?php echo ( RH_Reading::counter('closed') ) ? RH_Reading::counter('closed') : '0';?></span>Completed<br>Requests</span>
				<span class="unanswered bottom-1"><span class="count gold"><?php echo ( RH_Reading::counter('open') ) ? RH_Reading::counter('open') : '0';?></span>Unanswered<br>Requests</span>
				<span class="average bottom-1"><span class="count gold"><?php echo $average; ?></span>Average Rating<br>Received</span>
			</div>
		</div>
	</div>
	<div class="section section-2">
		<h4 class="section-heading">Your Availability this month</h4>
		<div id="calendar"></div>
		<div style="display: none" class="selected-date count"><?php echo $sd ?></div>
		<div class="vacation<?php echo $ol; ?>">Vacation Mode <span class="mode trans"></span></div>
	</div>
	<div class="section section-3">
		<h4 class="section-heading">You Have <?php echo RH_Reading::total(); ?> New Notifications</h4>
		<div class="section-inner">
			<span class="new-reading-count"><?php echo RH_Reading::counter('open'); ?> New Reading Requests</span>
			<a href="<?php echo RH_Rewrite::url('readings'); ?>" class="read-btn btn">Start Reading</a>
			<span class="followups-count"><?php echo RH_Reading::counter('in-progress'); ?> New Follow-Ups!</span>
			<a href="<?php echo RH_Rewrite::url('in-progress'); ?>" class="read-btn btn">Answer Follow-Ups</a>
			<span class="feedback-count"><?php echo RH_Reading::counter('archived'); ?> New Feedback Updates!</span>
			<a class="read-btn btn" href="<?php echo RH_Rewrite::url('archived'); ?>">View Feedback</a>
		</div>
	</div>
</div>
<script type="text/javascript">
var	rh_dates = <?php echo str_replace(array('\\', ' '), array('', ''), json_encode( $seldate ) );?>
</script>