<?php 	
if( !rh_role('reader') ) die('You aren\'t allowed to be here!');
$type = array(
	'meta_query' => array(
		'relation' => 'AND',
		array(
            'key' => '_rh_status',
			'value' => 'awiting_feedback',
			'compare' => '=',
        ),
        array(
            'key' => '_rh_assign_to',
			'value' => rh_user('ID'),
			'compare' => '=',
        )
	),
);
$readings = get_reading( $type );
$m = '<h3 align="center">You don\'t have any readings awaiting feedback</h3>';
?>
<div class="reading-content awaiting-feedback">
	<?php if ( !$readings ) {
		echo $m.'</div>';
		return false;
	} ?>
	<div class="wf-reading-view">
		<?php
		foreach ($readings as $reading => $key ) {
			$user = get_userdata( $key->post_author );
			$finish = '';
			$comments = get_comments( array(
			    'post_id' => $key->ID,
			    'orderby' => 'comment_date_gmt',
			    'status' => -1, 
			    'number' => 1 
			));
			if ( $comments ) $finish = date('M d, Y', strtotime( $comments[0]->comment_date ) );
			echo '<div class="reading-item clear" data-id="'.$key->ID.'">'.'<div class="poster-details awf">';
			if ( $user ) {
				echo rh_reader_data('imager', $user->ID, false ).'<span class="name">'.
					rh_reader_data('name', $user->ID, false ).'<i class="loc">'.
					rh_reader_data('location', $user->ID, false).'</i></span>';
			} else {
				echo '<span class="name">User No Longer Exist</span>';
			}
			echo '</div><div class="feedback-data">';
			echo '<span class="fdates gold">Reading Finish<span class="finish-date">'. $finish .'</span></span>';
			echo '<span class="freq"><button id="req-fw" class="btn req-fw" data-id="'. $key->ID .'">Request Feedback</button></span>';
			echo '<span class="arch"><button id="mto_arch" class="btn archived" data-id="'. $key->ID .'">ARCHIVE</button></span>';
			echo '</div></div>';
		} ?>
	</div>
</div>