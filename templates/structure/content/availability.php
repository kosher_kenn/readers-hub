<?php if( !rh_role('reader') ) die('You aren\'t allowed to be here!'); 
$ol = ( rh_reader_data('onleave') ) ? ' active' : '' ; 
$seldate = rh_reader_data('seldate');
$sd = '';
if ( is_array( $seldate ) ) {
	foreach ($seldate as $date => $count) {
		if ( strtotime( $date ) > strtotime ( '-1 day' ,time() ) ) {
			$sd .= '<p><input data-date="'.$date.'" type="number" step="1" min="0" max="10" value="'.$count.'" title="Qty"></p>';
		}
	}
}
$weekly = rh_reader_data('weekly');
$day = '';
if ( is_array( $weekly ) ) {
	foreach ($weekly as $week => $count) {
		$day .= '<p><span class="day">'.$week[0].'</span>
			<input type="button" value="-" class="minus">
				<input id="'.$week.'" type="number" step="1" min="0" max="10" value="'.$count.'" title="Qty">
				<input type="button" value="+" class="plus">
			</p>';
	}
} ?>
<div class="availability-content">
	<div class="vacation vac-opt<?php echo $ol; ?>">Vacation Mode <span class="mode trans"></span></div>
	<div class="section left float">
		<h4 class="section-heading">My Time Zone</h4>
		<?php echo RH_Help::timezone( rh_reader_data('timez'), 'rh-timezone' ); ?>
		<div id="calendar-opt"></div>

		<div class="selected-date count"><?php echo $sd ?>
			<input style="display:none" data-date="06/22/1988" type="number" step="1" min="0" max="10" value="0" title="Qty">
		</div>
	</div>
	<div class="section right float">
		<div class="date-select trans">
			<h4 class="section-heading">HOW MANY READINGS WILL YOU DO PER WEEK?</h4>
			<div class="count weekly">
			<?php if ( $day ) { echo $day; } else { ?>
			<p><span class="">S</span>
				<input type="button" value="-" class="minus">
				<input id="Sunday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">M</span>
				<input type="button" value="-" class="minus">
				<input id="Monday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">T</span>
				<input type="button" value="-" class="minus">
				<input id="Tuesday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">W</span>
				<input type="button" value="-" class="minus">
				<input id="Wednesday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">T</span>
				<input type="button" value="-" class="minus">
				<input id="Thursday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">F</span>
				<input type="button" value="-" class="minus">
				<input id="Friday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p>
			<p><span class="">S</span>
				<input type="button" value="-" class="minus">
				<input id="Saturday" type="number" step="1" min="0" max="10" value="0" title="Qty">
				<input type="button" value="+" class="plus">
			</p><?php } ?>
			</div>
			<button class="btn avail-update" data-action="bah">Save</button>
		</div>
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var	rh_dates = <?php echo str_replace(array('\\', ' '), array('', ''), json_encode( $seldate ) );?>
</script>