<?php
$editor_id = 'new_rh_reading';
$settings =   array(
    'wpautop' => true,
    'media_buttons' => true, 
    'textarea_name' => $editor_id, 
    'textarea_rows' => get_option('default_post_edit_rows', 5), 
    'tabindex' => '',
    'editor_css' => '', 
    'editor_class' => '', 
    'teeny' => false, 
    'dfw' => false, 
    'tinymce' => true, 
    'quicktags' => true 
); 
$readings = get_post( get_query_var('rh') );
$m = '<h3 align="center" style="padding-top: 50px">Readings don\'t exist!</h3>';
$op = ( rh_user('ID') === $readings->post_author ) ? true: false;
?>
<div class="reading-content reading-section clear normal-user <?php do_action('rh/content/classes', $readings); ?>">
	<?php if ( !$readings ) {
		echo $m.'</div>';
		return false;
	} 
	$assign = RH_Reading::read_meta('assign_to', $readings->ID ); 
	if ( $op || rh_user('ID') == $assign || rh_role('admin') ) {
	$feedback = RH_Reading::get_message($readings->ID, 'rh_feedback');
	$user = get_userdata( $assign ); 
	$poster = get_userdata( $readings->post_author ); ?>
	<div class="readers-profile clear">
		<?php if ( $user && ( rh_user('ID') === $readings->post_author || rh_role('admin') ) ) { ?>
		<span class="rcaption">Your Reader: </span> 
		<div class="reader-data"><?php echo rh_reader_data('imager', $user->ID ); ?>
			<span  class="reader-name"><?php echo rh_reader_data('name', $user->ID ); ?></span><br/>
			<span class="rrate"><?php echo rh_user_stat( $user->ID ); ?></span>
		</div>
		<a class="btn" href="<?php echo RH_Rewrite::url('index'). '/reader/' . $user->ID;?>">View Reader Profile</a>
		<?php } else if ( $assign && rh_user('ID') == $assign && $feedback ) { 
			$rating = _rh_get_star_rating( $readings->ID );
			echo '<div class="user-feedback"><div class="fd-user reader-data">'.
				rh_reader_data('imager', $poster->ID, false).'<span class="name">'.
				rh_reader_data('name', $poster->ID, false).'<i class="loc">'.
				rh_reader_data('location', $poster->ID, false).
				'</i><i class="gender">'.rh_reader_data('gender', $poster->ID, false).'</i><i class="bday">Born in '.
				rh_reader_data('birthday', $poster->ID, false).'</i></span>';
			echo '</div><div class="fd-data">';
			echo '<div class="feedback-red">'.wpautop($feedback[0]->comment_content).'</div>';
			echo '<span class="stars"><i></i><i></i><i></i><i></i><i></i>
				<span class="ratings">'.str_repeat('<i></i>', $rating).'</span></span>';
			echo '</div></div>';
		} else if ( $assign && rh_user('ID') == $assign ) { ?>
		<h3 align="center">You are assigned to this Reading</h3>
		<?php } else { ?>
		<h3 align="center">No User assign to this readings yet!</h3>
		<?php } ?>
	</div>
	<div class="reading-view">
		<?php
		$messages = RH_Reading::get_message( $readings->ID );
		echo '<div class="reading-msg active" id="reading-'.$readings->ID.'"><ul><li>'.
			'<span class="user-img"><a class="fancybox" href="'.rh_reader_data('image', $readings->post_author, false).
			'">'.rh_reader_data('imager', $readings->post_author, false) .'</a></span>'.
			'<i class="pdt rview">'.date('M d', strtotime( $readings->post_date ) ). '</i>'.
			'<div class="msg-cont">'. wpautop( $readings->post_content ).'</div></li>';
		if ( $messages ) {
			foreach ($messages as $message => $obj) {
				$user = ( rh_role( 'reader', $obj->user_id ) ) ? 'ftr-user' : 'normal-user' ;
				echo '<li class="responses '.$user.'"><span class="user-img"><a class="fancybox" href="'.rh_reader_data('image', $obj->user_id, false).'">'.rh_reader_data('imager', $obj->user_id, false) .'</a></span><i class="pdt rview">'.date('M d', strtotime( $obj->comment_date ) ). '</i>'. 
					'<div class="msg-cont">'. wpautop( $obj->comment_content ).'</div></li>';
			}
		}
		echo '</ul></div>';
		if ( $op || rh_user('ID') == $assign ) { 
			echo '<div class="user-action">';
			echo '<h6 class="wn_cap">What do you want to do Next?</h6>';
			echo '<button class="btn folloup">Send Follow-up </button>';
			if ( RH_Reading::read_meta('status', $readings->ID ) === 'awiting_feedback' && $op ) {
				echo ' or <button class="btn feedback">Leave Feedback </button>';
			}
			echo '</div>';
			echo '<duv class="fed-res">';
			echo '<div class="normal-response">';
			wp_editor( '', 'new_rh_reading', $settings );
			echo '<input type="hidden" id="editing-id" value="'. $readings->ID .'" />';
			echo '<button class="btn send-reading-response" id="send-followup">Send Message</button>';
			echo '</div>';
			if ( RH_Reading::read_meta('status', $readings->ID ) === 'awiting_feedback' && $op ) { ?>
				<div class="feedback-section">
					<p>Feedback</p>
					<div class="rating">
						<span class="star-rating">
							<span class="rcount" style="width: 100%"></span>
							<span class="voting"><span></span></span>
						</span>
						<p class="star-text">Excellent</p>
					</div>
					<input type="number" max="5" class="rate-count" value="5" />
					<textarea rows="5" cols="40" class="rating-content"></textarea>
					<button class="btn" id="leave-feedback">POST FEEDBACK</button>
				</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
	<?php } else {
		echo $m.'</div>';
		return false;
	} ?>

</div>
<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/js/fancybox/source/jquery.fancybox.css" type="text/css" media="all" />
<script src="<?php echo RH_URL ?>assets/js/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
(function($) {
	$(document).ready( function() {
		$(".fancybox").fancybox({'padding': 5});
	});
})(jQuery);
</script>