<?php if( !rh_role('reader') ) die('You aren\'t allowed to be here!'); 
$average = ( RH_Reading::average() ) ? RH_Reading::average() : '0'; ?>
<div class="profile-content profile">
	<a class="btn edit open-ajax" data-page="edit" href="<?php echo RH_Rewrite::url('edit'); ?>">Edit Profile</a>
	<div class="section section-1">
		<div class="user-info left float">
			<span class="user-avatar"><img src="<?php echo rh_reader_data('image'); ?>" alt="User"></span>
			<span class="reader-rate gold" style="margin-bottom: 0"><?php echo rh_user_stat(rh_user('ID')) ?></span>
			<span class="reader-name"><?php echo rh_reader_data('name'); ?></span>
			<span class="date-registered"><?php printf( 'Member Since %s',  date( "M Y", strtotime( rh_user('user_registered') ) ) ); ?></span>
			<span class="stars">
				<i></i><i></i><i></i><i></i><i></i>
				<span class="ratings"><?php echo str_repeat('<i></i>', $average ); ?></span>
			</span>
			<div class="user-request">
				<span class="completed bottom-1"><span class="count gold"><?php echo ( RH_Reading::counter('closed') ) ? RH_Reading::counter('closed') : '0';?></span>Completed<br>Request</span>
				<span class="unanswered bottom-1"><span class="count gold"><?php echo ( RH_Reading::counter('open') ) ? RH_Reading::counter('open') : '0';?></span>Unanswered<br>Request</span>
				<span class="average bottom-1"><span class="count gold"><?php echo $average; ?></span>Average Rating<br>Received</span>
			</div>
		</div>
		<div class="user-info right float">
			<div class="user-row">
				<p>
					<span class="key">Email:</span>
					<span class="value"><?php echo rh_reader_data('mail'); ?></span>
				</p>
				<p>
					<span class="key">Gender:</span>
					<span class="value"><?php echo rh_reader_data('gender'); ?></span>
				</p>
			</div>
			<div class="user-row">
				<p>
					<span class="key">Location:</span>
					<span class="value"><?php echo rh_reader_data('location'); ?></span>
				</p>
				<p>
					<span class="key">Time Zone</span>
					<span class="value"><?php echo rh_reader_data('timez'); ?></span>
				</p>
			</div>
			<div class="user-row bio">
				<span class="key">Biography</span>
				<?php echo wpautop( get_user_meta( rh_user('ID'), 'description', true) ); ?>
			</div>
		</div>
	</div>
</div>
<?php wp_enqueue_media(); ?>
