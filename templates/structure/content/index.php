<?php
$referrer = ( isset( $_GET['ref'] ) ) ? $_GET['ref'] : home_url(). '/readers-hub/';
echo '<div class="content-index">';
if ( !is_user_logged_in() ) {
	echo '<div class="login-section formwrap">';
	echo '<div class="login-right"><p>Log in below to access your free Tarot reading and communicate with your allocated Reader<br>Check your inbox for your login details.</p>';
	wp_login_form( array(
		'redirect'       => $referrer,
		'form_id'        => 'loginform',
		'id_username'    => 'user_login',
		'id_password'    => 'user_pass',
		'id_remember'    => 'rememberme',
		'id_submit'      => 'wp-submit',
		'label_username' => __( 'email' ),
		'label_password' => __( 'Password' ),
		'label_remember' => __( 'Remember Me' ),
		'label_log_in'   => __( 'Log In' ),
		'value_username' => '',
		'value_remember' => false
	));
	echo '<a href="#" class="showform" data-show=".lost_password" data-hide=".login-section">Lost your password?</a></div>';
	echo '<div class="free-reading"><p><img src="'.RH_URL.'assets/img/rh-login.png" alt="Req"/><a class="btn rev rqr" target="_blank" href="/free-tarot-readings">Get a Free Reading</a></p></div>';
	echo '</div>';
	echo '<div class="lost_password formwrap"><p>Enter your email address and we\'ll send you a link you can use to pick a new password.</p>';
 	echo '<form id="lostpasswordform" action="'.wp_lostpassword_url(). '" method="post">';
 	echo '<p class="profile "><label class="key" for="user_login">Email</label>';
 	echo '<span class="edit-content"><input type="text" name="user_login" id="user_login"></span></p>';
 	echo '<p><input type="submit" name="submit" class="btn" value="Reset Password"/><a href="#" class="showform backtolg" data-show=".login-section" data-hide=".lost_password">Back to Login</a></p>';
    echo '</form></div>';
} else {
	// default settings
	$user = wp_get_current_user();
	if ( rh_role('canbe_reader') && !rh_role('reader') ) {
		wp_redirect( RH_Rewrite::url('edit') ); exit;
	} else if( rh_role('reader') ) {
		wp_redirect( RH_Rewrite::url('dashboard') ); exit;
	} else if ( rh_role('poster') && !_rh_user_banned( rh_user('ID') ) ) {
		/*if ( !rh_customer_can_post( rh_user('ID') ) ) {
			echo '<p>Sorry, You can only request a reading once a month!</p>';
		} else if ( !rh_reader_select(true) ) { 
			echo '<p>Sorry, No reader is currently available!</p>';
		} else {
			$content = '';
			$editor_id = 'new_rh_reading';
			$settings =   array(
			    'wpautop' => true,
			    'media_buttons' => true, 
			    'textarea_name' => $editor_id, 
			    'textarea_rows' => get_option('default_post_edit_rows', 10), 
			    'tabindex' => '',
			    'editor_css' => '', 
			    'editor_class' => '', 
			    'teeny' => false, 
			    'dfw' => false, 
			    'tinymce' => true, 
			    'quicktags' => true 
			);
			wp_editor( $content, $editor_id, $settings );
			echo '<button class="btn" id="new-reading">Send Message</button>';
		}*/
		wp_redirect( RH_Rewrite::url('request') ); exit;		
	} else if ( rh_role('poster') && _rh_user_banned( rh_user('ID') ) ) {
		echo 'Your account is Banned!';	
	} else {
		echo 'Nothing Here';
	}
}
echo '</div>';