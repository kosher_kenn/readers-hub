<?php 
$reader = RH_Reading::reader( get_query_var( 'rh_username' ), 'ID' );
$average = ( $reader && RH_Reading::average(  $reader->ID ) ) ? RH_Reading::average(  $reader->ID ) : '0';
?>
<div class="profile-content profile">
	<div class="section section-1">
		<?php if ( !$reader || !rh_role('reader', $reader->ID ) ) {
			echo '<h3 align="center">User Not Found!</h3>';
			return '</div></div>'; 
		} ?>
		<div class="user-info left float">
			<span class="user-avatar"><?php echo rh_reader_data('imager', $reader->ID); ?></span>
			<div><span class="reader-name"><?php echo rh_reader_data('name', $reader->ID); ?></span></div>			
		</div>
		<div class="user-info right float">
			<div class="user-row bio">
				<span class="key">Biography</span>
				<?php echo wpautop( get_user_meta( $reader->ID, 'description', true) ); ?>
			</div>
		</div>
	</div>
</div>