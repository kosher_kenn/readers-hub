<?php 
if( !rh_role('reader') ) die('You aren\'t allowed to be here!'); 	
$type = array(
	'meta_query' => array(
		'relation' => 'AND',
		array(
            'key' => '_rh_status',
			'value' => 'closed',
			'compare' => '=',
        ),
        array(
            'key' => '_rh_assign_to',
			'value' => rh_user('ID'),
			'compare' => '=',
        )
	),
);
$readings = get_reading( $type );
$m = '<h3 align="center">You don\'t have any archived readings</h3>';
?>
<div class="reading-content archived-section">
	<?php if ( !$readings ) {
		echo $m.'</div>';
		return false;
	} ?>
	<div class="archive-reading-view">
		<?php
		foreach ($readings as $reading => $key ) {
			$user = get_userdata( $key->post_author );
			$feedback = RH_Reading::get_message($key->ID, 'rh_feedback');
			$rating = _rh_get_star_rating( $key->ID );
			echo '<div class="reading-item clear" data-id="'.$key->ID.'">'.
			'<div class="poster-details"><i class="pdt">'.
			date('M d', strtotime( $key->post_date ) ). '</i>';
			if ( $user ) {
				echo rh_reader_data('imager', $user->ID, false).'<span class="name">'.
			rh_reader_data('name', $user->ID, false).'<i class="loc">'.rh_reader_data('location', $user->ID, false).'</i></span>';
			} else {
				echo '<span class="name">User no longer exist</span>';
			}			
			echo '<span class="stars"><i></i><i></i><i></i><i></i><i></i>
				<span class="ratings">'.str_repeat('<i></i>', $rating).'</span></span>';
			echo '</div><div class="feedback-data">';
			if ( isset( $feedback[0] ) ) 
				echo '<div class="fd-con trans">'.wpautop($feedback[0]->comment_content).'</div>';
			echo '<a class="btn" href="'.RH_Rewrite::url('readings')
				.'/'.$key->ID.'">View Reading</a>';
			echo '</div></div>';
		} 
	?>
	</div>
</div>