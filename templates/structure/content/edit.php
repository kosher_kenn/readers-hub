<?php if( rh_role('reader') || rh_role('canbe_reader') ) { 
$text = (rh_role('reader') ) ? 'Save Profile': 'Become a Reader';
$be_reader =  (rh_role('canbe_reader') ) ? ' data-become="reader"': '';?>
<div class="edit-content profile">
	<button class="btn edit" id="save_profile" data-action="save" href="#"<?php echo $be_reader?>><?php echo $text; ?></button>
	<div class="section section-1">
		<div class="user-info left float">
			<span class="user-avatar"><img src="<?php echo rh_reader_data('image'); ?>" alt="User"></span>
			<input id="ftr_ava" type="hidden" value="<?php echo rh_reader_data('image'); ?>" />
			<a class="new-ava" id="ftr_ava_button" href="#">Upload New Photo</a>
			<span class="key name">Name: </span>
			<span class="reader-name">
				<input type="text" id="ftr_name" class="user-name" value="<?php echo rh_reader_data('name'); ?>" />
			</span>
			<?php 
				if( rh_role('reader') ) {
					echo '<button class="btn deact aload" data-action="deactivate">Deactivate Profile</button>';
				}
			?>
		</div>
		<div class="user-info right float">
			<div class="user-row">
				<p>
					<span class="key">Email:</span>
					<span class="value"><input type="text" id="user-email" value="<?php echo rh_reader_data('mail'); ?>" /></span>
				</p>
				<p>
					<span class="key">Gender:</span>
					<span class="value"><?php echo rh_gender_select( rh_reader_data('gender')) ?></span>
				</p>
			</div>
			<div class="user-row">
				<p>
					<span class="key">Location:</span>
					<span class="value"><?php echo RH_Help::country( rh_reader_data('location') ) ?> </span>
				</p>
				<p>
					<span class="key">Time Zone</span>
					<?php echo RH_Help::timezone( rh_reader_data('timez') ); ?>
				</p>
			</div>
			<div class="user-row bio">
				<span class="key">Biography</span>
				<textarea row="5" cols="50" id="user_bio"><?php echo get_user_meta( rh_user('ID'), 'description', true); ?></textarea>
			</div>
		</div>
	</div>
	
</div>
<?php wp_enqueue_media(); ?>
<script type="text/javascript">
var profile_edit = '<?php echo RH_URL ?>assets/js/rh-media-upload.js';
$(document).ready( function() {
	$.rhRequire( profile_edit, 'profile_edit_checker' );
});
</script>
<?php } else {
	 die('You aren\'t allowed to be here!'); 
}