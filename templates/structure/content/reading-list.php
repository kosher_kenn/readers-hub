<?php 
if( !rh_role('admin') ) die('You aren\'t allowed to be here!');	

$search = isset( $_GET['search'] ) ? $_GET['search'] : false;
$page = ( get_query_var('rr_list') ) ? get_query_var('rr_list') : 1;

$arg = array(
	'post_type'         => 'readers_hub',
	'posts_per_page'    => 20,
	'orderby'           => 'date',
	'paged'				=> $page,
	'order'             => 'DESC',
	'meta_query' => array(
		'relation' => 'AND',
		array(
            'key' => '_rh_status',
			'value' => array('open','assigned','inprogress'),
			'compare' => 'IN',
        ),
	),
);

if ( $search ) { 
	$arg['s'] = $search;
}

$query = new WP_QUery( $arg ); 
$readings = $query->get_posts();

$page_args = array(
	'base'               => RH_Rewrite::url('reading-list') . '%_%',
	'format'             => '/%#%',
	'total'              => $query->max_num_pages,
	'current'            => $page,	
	'prev_text'          => __('«'),
	'next_text'          => __('»'),
);

#echo '<pre>', print_r( $query, 1), '</pre>';
$data = '';
if ( $readings ) {
	foreach ($readings as $readings => $info ) {
		$link = RH_Rewrite::url('readings'). '/'.$info->ID;
		$reader = RH_Reading::read_meta( 'assign_to', $info->ID );
		$poster = get_post_field( 'post_author', $info->ID );
		$assignto = ( $reader ) ? '<i class="name">'.rh_reader_data('name', $reader, false ).'<u>'.rh_user_stat($reader).'</u></i>': '<i style="line-height:50px;padding-left: 50px;">No Reader Assign</i>';
		$due_date = date('Ymd', strtotime( RH_Reading::due_date( $info->ID ) ) );
		$data .= '<li class="single-reading user-item" id="reading-'.$info->ID.'" data-id="'.$info->ID.'" data-reader="'.$reader.'" data-rname="'.rh_reader_data('name', $reader, false ).'" data-customer="'. rh_reader_data('name', $poster, false ).'" data-status="'. RH_Reading::status( get_post_meta($info->ID, '_rh_status',true), false ).'" data-ddate="'.$due_date.'">';
		$data .= '<span class="ul-reader item">'.$assignto.'</span>';
		$data .= '<span class="ul-user item">'.'<i class="name">'. rh_reader_data('name', $poster, false ).'</i></span>';
		$data .= '<span class="ul-dd item">'. RH_Reading::status( get_post_meta($info->ID, '_rh_status',true), false ).'</span>';
		$data .= '<span class="ul-ar item"><a class="gold" href="'.$link.'">'.RH_Reading::due_date( $info->ID ).'</a></span>';
		$data .= '<span class="ul-cr item"><i data-action="reassign" class="reassign trans"></i></span>';
		$data .= '<span class="ul-cr item"><i data-action="delete" class="deleter trans"></i></span>';
		$data .= '</li>';
	}
}
?>
<div class="reading-content reading-list-section">
	<form action="" method="GET" class="item-search">
		<input class="sbox" type="text" name="search" placeholder="Search Readings..." value="<?php echo $search; ?>" />
		<input type="Submit" value="Go" />
	</form>
	<ul class="reading-lists list-format">
		<li class="list-heading">
			<span class="ul-reader item"><i class="sortit" data-order="rname">Reader</i></span>
			<span class="ul-user item"><i class="sortit" data-order="customer">Customer</i></span>
			<span class="ul-dd item"><i class="sortit" data-order="status">Reading Status</i></span>
			<span class="ul-ar item"><i class="sortit" data-order="ddate">Due Date</i></span>
			<span class="ul-cr item">Re-assign</span>
			<span class="ul-cr item">Delete</span>
		</li>
		<?php echo $data; ?>
	</ul>
	<div class="res-nav"><?php echo paginate_links( $page_args ); ?></div>
	<div id="user-edit" class="user-editor" style="display:none;">
		<span class="edit-icon"></span>
		<p class="edit-note" align="center"></p>
		<p class="edit-subnote" align="center"></p>
		<button class="btn close-edit auto">Cancel</button>
		<button id="reading-update" data-action="" data-user="" class="btn close-edit btn-action">Delete</button>
		<input type="hidden" id="manual-user" val="" />
	</div>
</div>
<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/js/fancybox/source/jquery.fancybox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo RH_URL; ?>assets/js/select2/css/select2.min.css" type="text/css" media="all" />
<script type="text/javascript">
var fancyboxjs		= '<?php echo RH_URL ?>assets/js/fancybox/source/jquery.fancybox.pack.js',
	select2js		= '<?php echo RH_URL ?>assets/js/select2/js/select2.min.js',
	readinglistjs	= '<?php echo RH_URL ?>assets/js/rh-reading-list.js';
(function($) {
	$(document).ready( function() {
		$.rhRequire( fancyboxjs, 'fancybox_checker' );
		$.rhRequire( select2js, 'select2_load_checker' );
		$.rhRequire( readinglistjs, 'reading_list_load_checker' );
	});
})(jQuery);
</script>


		