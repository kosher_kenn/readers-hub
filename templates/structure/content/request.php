<?php 	
$type = array(
	'meta_key'		=> '',
	'meta_value'	=> '',
	'author' => rh_user('ID'),
);
$readings = get_reading( $type );
$m = '<h3 align="center">You don\'t have any reading request</h3>';
?>
<div class="reading-content archived-section">
	<?php if ( !$readings ) {
		echo $m;
	} else { 
		echo '<div class="archive-reading-view">';
		foreach ($readings as $reading => $key ) {
			$user = get_userdata( $key->post_author );
			$feedback = RH_Reading::get_message($key->ID, 'rh_feedback');
			#$rating = (int) get_comment_meta( $feedback[0]->comment_ID, 'rh_rating', true );
			echo '<div class="reading-item clear" data-id="'.$key->ID.'">'.
			'<div class="poster-details"><i class="pdt">'.
			date('M d', strtotime( $key->post_date ) ). '</i>'.
			rh_reader_data('imager', $user->ID, false).'<span class="name">'.
			rh_reader_data('name', $user->ID, false).'</span>';
			#echo '<span class="stars"><i></i><i></i><i></i><i></i><i></i>
			#	<span class="ratings">'.str_repeat('<i></i>', $rating).'</span></span>';
			#echo '<pre>', print_r( $rating, 1 ), '</pre>';
			echo '</div><div class="feedback-data">';
			#echo '<div class="fd-con trans">'.wpautop($feedback[0]->comment_content).'</div>';
			echo '<a class="btn" href="'.RH_Rewrite::url('readings').'/'.$key->ID.'">View Reading</a>';
			echo '</div></div>';
		} 
		echo '</div>';
	} ?> 
</div>