;(function($) {
	'use strict';
	$(document).ready( function() {
		$('#calendar').datepicker(seldates());
		$('#calendar-opt').multiDatesPicker( cald_opt(2) );

		$('.fd-con').height( 35 );
		setTimeout(function(){ 
			$('.fd-con').height( '' );
			$('.fd-con').each( function() {
				var dheight = $(this).outerHeight(true);
		    	if ( dheight > 60 ) {
		    	    $(this).after('<a href="#" class="read-more" data-height="'+ dheight +'">Read More</a>');
		    	    $(this).addClass('maxheight');
		    	}
			});
		}, 500);
	});

	$('body').on('click', '.feedback-data .read-more', function(e) {
		e.preventDefault();
		$(this).parent().find('.fd-con').toggleClass('maxheight').css('height', $(this).data('height') );
		if ($(this).text() == "Read More") $(this).text("Hide Feedback")
		else $(this).text("Read More");
	});

	$('body').on('click', '.side-nav.first li:not(.start):not(.readings):not(.profile):not(.members-list) a, a.open-ajax', function(e) {
		e.preventDefault();
		var current = window.location.href,
			me 		= $(this).parent(),
			page 	= $(this).attr('href') + '/';
		if ( current !== page ) {
			$('.page-heading, .page-content').removeClass('fadeIn');
			$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
			$('.nav-wrapper').slideUp('normal', function() { $(this).remove(); } );
			$.ajax({
				type: 'POST',
				url: rh_ajax.url,
				data: {
					'action'	: 'rh_page_req',
					'security'	: rh_ajax.sec,
					'page_req'		: $(this).data('page'),
				},
				success: function(data) { 
					$('.ajax-loader').remove();
					if ( data.subnav ) $(data.subnav).insertAfter('.header-area').find('li:first-of-type').addClass('active');
					if ( data.page ) {
						$('.side-nav.first li').removeClass('active');
						$('.side-nav.first li.'+ data.page ).addClass('active');
					}
					window.history.pushState({ id: data.page }, 'Readers Hub | ' + rhcap( data.page ), page);
					document.title = 'Readers Hub | ' + rhcap( data.page );
					class_mod('body', 'page-' + data.page);
					if ( data.title ) $('.page-heading').html( data.title ).addClass('animated fadeIn');
					if ( data.content ) $('.page-content').html( data.content ).addClass( 'animated fadeIn' );
					if ( me.hasClass('dashboard') ){
						$('#calendar').datepicker('destroy').datepicker(seldates()).focus();
					}
					if ( me.hasClass('availability') ) {
						$('#calendar-opt').multiDatesPicker('destroy').multiDatesPicker(
							cald_opt(2)
						).focus();
					} 
					rh_content_resize();
				},
				error: function(errorThrown) {
					$('.ajax-loader').remove();
					console.log( errorThrown );
				},
			});
		}
		
	});

	$('body').on('click', '.admin-nav51ag li a', function(e) {
		e.preventDefault();
		var current = window.location.href,
			me 		= $(this).parent(),
			page 	= $(this).attr('href') + '/';
		if ( current !== page ) {
			$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
			$.ajax({
				type: 'POST',
				url: rh_ajax.url,
				data: {
					'action'	: 'rh_page_req',
					'security'	: rh_ajax.sec,
					'page_req'		: $(this).data('page'),
				},
				success: function(data) { 
					$('.ajax-loader').remove();
					if ( data.page ) {
						$('.admin-nav li').removeClass('active');
						$('.admin-nav li.'+ data.page ).addClass('active');
					}
					window.history.pushState({ id: data.page }, 'Readers Hub | ' + rhcap( data.page ), page);
					document.title = 'Readers Hub | ' + rhcap( data.page );
					class_mod('body', 'page-' + data.page);
					if ( data.content ) $('.page-content').html( data.content ).addClass( 'animated fadeIn' );
				},
				error: function(errorThrown) {
					$('.ajax-loader').remove();
					console.log( errorThrown );
				},
			});
		}
	});

	$('body').on('click', '.aload', function(e) {
		e.preventDefault();
		$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
			$.ajax({
				type: 'POST',
				url: rh_ajax.url,
				data: {
					'action'	: 'rh_page_req',
					'security'	: rh_ajax.sec,
					'rh_do'		: $(this).data('action'),
				},
				success: function(data) { 
					$('.site-content').before('<div class="notice">'+ data.status +'<i></i></div>');
					$('.ajax-loader').remove();
				 },
				error: function(errorThrown) {
					$('.ajax-loader').remove();
					//console.log( errorThrown );
				},
			});
	});

	$(window).bind('popstate', function(e) {
		if( $('body').hasClass('history') ) { 
			//console.log( window.history.state['id'] );
		} 
	});

    $('body').on('click', '.notice i', function(e){
    	$(this).parent().remove();
    });

	$('body').on('mouseenter mouseleave', 'td.ui-state-highlight:not(.onvacation)', function( e ) {
		var sd = n( ( $(this).data('month') + 1 ) )+'/'+ n( $(this).text() ) +'/'+$(this).data('year');
		if ( e.type === 'mouseenter' ) {
			$(this).append('<span class="num-r">'+ $('.selected-date').find('input[data-date="'+sd+'"]').val()+'</span>' );
		} else {
			$(this).find('.num-r').remove();
		}
        
    });

	// Availablity 
	$('body').on('click', '.frequent .vacation', function() {
		$('.frequent .vacation').removeClass('active');
		$(this).addClass('active');
	});

	$('body').on('click', '.vac-opt', function() {
		$(this).toggleClass('active');
	});

	$('body').on('click', '.count .plus', function(e) {
        e.preventDefault();
        var item = $(this).parent().find('input[type="number"]'),
            limit = ( item.attr('max') ) ? item.attr('max') : 99,
            value = parseInt( item.val() );
        if ( !isNaN( value ) &&  value < limit ) {
            item.val(value + 1);
            item.trigger('change');
        } 
    });

    $('body').on('click', '.count .minus', function(e) {
        e.preventDefault();
        var item = $(this).parent().find('input[type="number"]'),
            value = parseInt( item.val() );
        if ( !isNaN( value ) && value > 0 ) {
            item.val(value - 1);
            item.trigger('change');
        }
    });

   	$('body').on('click', '.avail-update', function() {
    	var weekly = {},
    		select_date = {},
			me = $(this),
    		onleave = ( $('.vac-opt').hasClass('active') ) ? 1 : 0 ;
    	$('.weekly input[type=number]').each( function() {
    		weekly[$(this).attr('id')] = $(this).val();
    	});
    	$('.selected-date input[type=number]').each( function() {
    		select_date[$(this).data('date')] = $(this).val();
    	});
    	console.log( select_date );
    	//return false;
    	$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
		me.prop('disabled', true);
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'profile_save',
				'rh_weekly'	: weekly,
				'rh_sd'		: select_date,
				'rh_leave'	: onleave,
				'rh_timez'	: $('#user-timez').val(),
			},
			success: function(data) { 
				me.prop('disabled', false);
				$('.ajax-loader').remove();
				$('.site-content').before('<div class="notice">'+ data.status +'<i></i></div>');
				setTimeout(function() {
					$('.notice').slideUp('normal', function() { $(this).remove(); } );
				}, 3000);
			},
			error: function(errorThrown) {
				me.prop('disabled', false);
				$('ajax-loader').remove();
				console.log( errorThrown );
			},
		});
   	});

	$('body').on('click', '#save_profile', function(e) {
		e.preventDefault();
		var become = ( $(this).data('become') ) ? $(this).data('become') : false,
			me = $(this);
		$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
		me.prop('disabled', true);
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'profile_save',
				'rh_uname'	: $('#ftr_name').val(),
				'rh_uava'	: $('#ftr_ava').val(),
				'rh_bio'	: $('#user_bio').val(),
				'rh_email'	: $('#user-email').val(),
				'rh_timez'	: $('#user-timez').val(),
				'rh_gender'	: $('#user-gender').val(),
				'rh_loc'	: $('#rh-country').val(),
				'rh_become'	: become,
			},
			success: function(data) { 
				$('.ajax-loader').remove();
				if ( data.status === 'profile_updated' ) {
					window.location.href = data.link;
				} else {
					me.prop('disabled', false );
				}
			},
			error: function(errorThrown) {
				$('ajax-loader').remove();
				console.log( errorThrown );
			},
		});
	});

	$('body').on('click', '#user-update, #reading-update', function(e) {
		var user = $(this).data('user'),
			action = $(this).data('action'),
			dat = $(this);
		if ( !user || !action ) return false; 
		$('.user-editor').prepend('<div class="ajax-loader"><span class="spinner">');
		dat.prop('disabled', true);
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'rh_mod_admin',
				'rh_mod'	: action,
				'rh_user'	: user,
			},
			success: function(data) {
				dat.prop('disabled', false);
				$('.ajax-loader').remove();
				if ( data.type === 'remove' ) {
					$('.users-lists #user-'+data.user ).slideUp('normal', function() { 
						setTimeout(function() { $(this).remove(); }, 3000 )
					});
				} else if ( data.type === 'suspend' ) {
					$('.page-content').trigger('click');
					$('.users-lists #user-'+data.user ).find('.suspend').removeClass('suspend').addClass('suspended');
				} else if ( data.type === 'unsuspend' ) {
					$('.page-content').trigger('click');
					$('.users-lists #user-'+data.user ).find('.suspended').removeClass('suspended').addClass('suspend');
				} else if ( data.type === 'delete' && data.status.ID ) {
					$('.page-content').trigger('click');
					$('.reading-lists #reading-'+data.status.ID ).slideUp('normal', function() { 
						setTimeout(function() { $(this).remove(); }, 3000 )
					});
				}
				$('.fancybox-close').trigger('click');
			},
			error: function(errorThrown) {
				dat.prop('disabled', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});
	});

	$('body').on('click', '.showform', function(e) {
		e.preventDefault();
		var show = $(this).data('show'),
			hide = $(this).data('hide')
		$(hide ).slideUp();
		$(show ).slideDown();
	});

	$('body').on('submit', '#create_new_customer', function(e) {
		e.preventDefault();
		var data = $(this).serialize(),
			btn = $(this).find('.btn');
		if ( !$(this).find('#user_email').val() ) return false;
		$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
		btn.prop('disabled', true );
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: data,
			success: function(data) {
				$('.ajax-loader').remove();
				if ( data.errors ) {
					btn.prop('disabled', false );
					var err = data.errors[Object.keys(data.errors)[0]];
					$('.notify').html('<div class="notice error">'+ err +'<i></i></div>');
				} else {
					window.location.href = data.url;
				}
			},
			error: function(errorThrown) {
				dat.prop('disabled', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});
	});

	$('body').on('click', '.dashboard-content #calendar, .dashboard-content .ui-datepicker', function() {
		var link = $('.side-nav .availability a').attr('href');
		window.location.href = link;
	});

	$('body').on('click', '#req-fw', function() {
		var me = $(this),
			id = me.data('id');
		me.prop('disabled', true);
		$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'request_feedback',
				'rh_reading': id,
			},
			success: function(data) {
				$('.ajax-loader').remove();
				if ( data.status ) {
					me.text('Feedback Requested');
				} else {
					me.prop('disabled', false);
					me.text('Problem Requesting Feedback');
				}
			},
			error: function(errorThrown) {
				me.prop('disabled', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});		
	});

	$('body').on('click', '#mto_arch', function() {
		var me = $(this),
			id = me.data('id');
		me.prop('disabled', true);
		$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'move_to_archived',
				'rh_reading': id,
			},
			success: function(data) {
				$('.ajax-loader').remove();
				if ( data.status ) {
					me.closest('.reading-item').slideUp();
				} 
			},
			error: function(errorThrown) {
				me.prop('disabled', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});		
	});
	
    $('body').on('click', '.list-heading .sortit', function() {
    	var orderby = $(this).attr('data-order'),
    		i = $(this);
		$('.list-heading .sortit').removeClass('active');
		i.addClass('active');

		$('.list-format .user-item').sort(function(a,b) {
			var an = $(a).data( orderby ),
				bn = $(b).data( orderby );
			if ( i.hasClass('asc') ) { 
				if(an > bn) return -1;
				if(an < bn) return 1;
				return 0;
			} else {
				if(an > bn) return 1;
				if(an < bn) return -1;
				return 0;
			}
		}).detach().appendTo('.list-format');
		if ( i.hasClass('asc') ) i.removeClass('asc').addClass('desc');
		else i.removeClass('desc').addClass('asc');
    });

	function cald_opt( count, opt ) {
		var num = ( count ) ? count : 1;
		return {
		    inline: true,
		    minDate: -1,
		    numberOfMonths: num,
		    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		    onSelect: function(dateText, inst) { 
				var dates = '<p class="added"><span class="sltcd">'+rh_date( dateText )+'</span><input type="button" value="-" class="minus"><input data-date="'+ dateText +'" type="number" step="1" min="0" max="10" value="1" title="Qty"><input type="button" value="+" class="plus"></p>';
				$('.selected-date').find( '.added' ).removeClass('added');
				if ( $('.selected-date').find('input[data-date="'+ dateText +'"]').length ) {
				    $('input[data-date="'+ dateText +'"]').parent().remove();
				} else {
					$('.selected-date').append( dates );
				}
			},
			beforeShowDay: function(date){
				var string = $.datepicker.formatDate('mm/dd/yy', date), i;
	            for (i = 0; i < rh_selected_dates().length; i++) {
	                if($.inArray( string, rh_selected_dates() ) !== -1) {
	                	if ( rh_dates[ string ] > 0 ) {
	                		return [true, '', ''];
	                	} else {
	                		return [true, 'onvacation', ''];
	                	}
	                }
	            }
	            return [true];
		    },
			addDates: rh_selected_dates(),
		    showOn: 'focus',
		};
	}

	function rh_selected_dates() {
		var	newdates = ['06/06/1912'];
		if ( typeof rh_dates !== 'undefined' ) {
			for ( var key in rh_dates ) {
				newdates.push(key);
			}
		}
		//console.log(newdates);
		return newdates;
	}
		

	function seldates() {
		return {
			inline: true,
			minDate: -1,
			//defaultDate: +7,
		    beforeShowDay: function(date){
				var string = $.datepicker.formatDate('mm/dd/yy', date), i;
	            for (i = 0; i < rh_selected_dates().length; i++) {
	                if($.inArray( string, rh_selected_dates() ) !== -1) {
	                	if ( rh_dates[ string ] > 0 ) {
	                		return [true, 'ui-state-highlight', ''];
	                	} else {
	                		return [true, 'ui-state-highlight onvacation', ''];
	                	}
	                }
	            }
	            return [true];
		    },
		    onSelect: function(dateText, inst) { 
				var link = $('.side-nav .availability a').attr('href');
				window.location.href = link;
			},
		}
	}

    function rh_date( date ) {
    	var monthNames = ["January", "February", "March", "April", "May", "June",
			  "July", "August", "September", "October", "November", "December"
			],
			obj = new Date( date ),
			d = obj.getDate(),
			m =  monthNames[obj.getMonth()],
			y = obj.getFullYear();
		return d + ' ' + m + ' ' + y;
    }

    function n(n){
	    return n > 9 ? "" + n: "0" + n;
	}

	function class_mod( elem, add) {
		$( elem ).removeClass( function (index, css) {
			return (css.match (/(^|\s)page-\S+/g) || []).join(' ');
		}).addClass( add + ' history');
	}

	function rhcap(string) {
		if ( string == undefined ) return;
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function rh_content_resize() {
		var p = ( !$('body').hasClass('page-readings') ) ? 335 : 385; 
	    var winH = $(window).height() - p ;
	    $('.page-content').css({
	        //'max-height': winH
	    });
	}

	window.addEventListener('resize', rh_content_resize );
	rh_content_resize();	

	$.rhRequiredecap = function( url, checker, callback, options ) {
		options = $.extend( options || {}, {
			dataType: "script",
			cache: true,
			url: url,
			success : callback
		});
		if ( typeof window[ checker ] === 'undefined' ) {
		    $.ajax( options );		
		    window[ checker ] = true;    
		}
	};
})(jQuery);