;(function($) {
	'use strict';
	$(document).ready( function() {
		var file_frame; // variable for the wp.media file_frame
		
		// attach a click event (or whatever you want) to some element on your page
		$( '#ftr_ava_button' ).on( 'click', function( event ) {
			event.preventDefault();
			if ( file_frame ) {
				file_frame.open();
				return;
			} 
			file_frame = wp.media.frames.file_frame = wp.media({
				title: $( this ).data( 'uploader_title' ),
				button: {
					text: $( this ).data( 'uploader_button_text' ),
				},
				multiple: false 
			});
			file_frame.on( 'select', function() {
				var attachment = file_frame.state().get('selection').first().toJSON();
				$('.user-avatar').html('<img src="'+ attachment.url +'" alt="User Avatar" />');
				$('#ftr_ava').val( attachment.url );
			});
			file_frame.open();
		});
	});
})(jQuery);