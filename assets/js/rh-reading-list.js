//var rh_reading_list_js = true;
;(function($) {
	'use strict';
	var select = '';
	$('body').on('click', '.deleter, .reassign', function(e) {
		var user = $(this).closest('.single-reading').attr('data-id'),
			action = $(this).data('action'),
			reader = $(this).closest('.single-reading').attr('data-reader'),
			dat    = $(this),
			ednote = ( action === 'reassign' ) ? 'Would you like to re-assign this reading automatically or manually?' : 'Are you sure you want to detelete this reading?',
			message = ( action === 'delete' ) ? 'This action is irreversable' : '';
		if ( action === 'reassign') {
			$('.site-content .wrap').prepend('<div class="ajax-loader"><span class="spinner">');
			$.ajax({
				type: 'POST',
				url: rh_ajax.url,
				data: {
					'action'	: 'rh_page_req',
					'security'	: rh_ajax.sec,
					'rh_do'		: 'rh_mod_admin',
					'rh_mod'	: action,
					'rh_user'	: user,
					'rh_reader'	: reader,
				},
				success: function(data) {
					$('.ajax-loader').remove();
					if ( data.rlist ) {
						$('.edit-note').html( ednote );
						select = data.rlist;
						$('.auto').removeClass('close-edit');
					} else {
						$('.user-editor').addClass('no-opt');
						$('.auto').attr('id', '').addClass('close-edit').text( 'Close' );
						$('.edit-note').html( 'No Reader Available' );
					}
					rh_load_popup();
				},
				error: function(errorThrown) {
					dat.prop('disabled', false);
					$('.ajax-loader').remove();
					console.log( errorThrown );
				},
			});
			$('.auto').attr('id', 'auto-reassign').attr('data-user', user ).attr('data-cread', reader).text( 'Automatically' );
			$('.btn-action').attr('data-action', 'request-reader' ).attr('data-cread', reader).attr('data-user', user).removeClass('close-edit')
				.attr('id', 'manual').text( 'Manually' );
		}
		else {
			$('.btn-action').attr('data-action', action ).attr('data-user', user).text( action );
			$('.edit-note').html( ednote );
			$('.edit-subnote').html( message );
			setTimeout(function() { rh_load_popup(); }, 20 );
		}
	});

	$('body').on('click', '.close-edit', function() {
		$('.fancybox-close').trigger('click');
	});
	
	$('body').on('click', '#manual', function() {
		$('.auto').attr('id', '').attr('data-post', '' ).addClass('close-edit').text( 'Cancel' );
		$('.edit-note').html( 'Choose Your Reader:' + select ).find("#reader-list").select2({
			templateResult: rh_select_temp
		});
		$(this).attr('id', 'manual-reassign').attr('data-action', 'manual_reassign').text('Choose this Reader');
		$('#manual-user').val( $('#reader-list').val() );
	});

	$('body').on('click', '#manual-reassign, #auto-reassign', function() {
		var dat		= $(this),
			user	= ( $('#manual-user').val() ) ? $('#manual-user').val() : false,
			action 	= 'reassign_reading',
			post	= $(this).data('user');
		dat.prop('disabled', true);
		$('.user-editor').prepend('<div class="ajax-loader"><span class="spinner">');
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_page_req',
				'security'	: rh_ajax.sec,
				'rh_do'		: 'rh_mod_admin',
				'rh_mod'	: action,
				'rh_user'	: user,
				'rh_post'	: post,
				'rh_reader'	: dat.data('cread'),
			},
			success: function(data) {
				dat.prop('disabled', false);
				$('.ajax-loader').remove();
				$('.edit-note').html( data.notice );
				$('.btn-action').attr('data-action', '' ).attr('data-user', '').attr('id', 'reading-update');
				$('.user-editor').addClass('no-opt');
				$('.auto').attr('id', '').addClass('close-edit').text('Close');
				if ( data.status ) {
					$('#reading-' + post).attr('data-reader',data.rid);
					$('#reading-' + post).find('.ul-reader').html( data.reader );
					$('#reading-' + post).find('.ul-ar').html( data.due_date );
					$('#reading-' + post).find('.ul-dd').text( 'FollowUp' );
				}
				setTimeout(function() { $('.fancybox-close').trigger('click'); }, 2000 );

			},
			error: function(errorThrown) {
				dat.prop('disabled', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});		
	});

	$('body').on('change', '#reader-list', function() {
		$('#manual-user').val( $(this).val() ); 
	});

	$(document).ready( function() {
		$('#reader-list').select2({
			templateResult: rh_select_temp
		});
		$('#rh-country').select2();
		$('#bdate').datepicker({
			changeMonth: true,
	        changeYear: true,
	        dateFormat: 'MM dd, yy',
	        maxDate: "-10y",
	        minDate: "-100y",
	        yearRange: "-100y:-10y",
	        defaultDate : '-18y',
		});
	});
	$('body').on('change', '#tos', function() {
		var verified = $('#verified').val();
		if ( $(this).is(':checked') ) { 
			if ( verified == 'no' ) $('#tos').val('yes');
			else $('#rh-submit').prop('disabled', false);
		} else {
			$('#tos').val('no');
			$('#rh-submit').prop('disabled', true);
		}
	});

	$('body').on('click', '#upload_photo', function(e) {
		e.preventDefault();
		$('#rh-img').trigger('click');
	});

	$('body').on('change', '#rh-img', function(e) {
		loadvalue( this );
	});

	$('body').on('click', '.showforms button', function(e) {
		var div = $(this).data('div');
		if ( $(this).hasClass('active') ) return false;
		$('.showforms button').removeClass('active');
		$(this).addClass('active');
		$('.rhfrom-data').slideUp().removeClass('active');
		$(div).slideDown().addClass('active');
	});

	function rh_only_18above( date ) {
		var today = new Date();
	    var birthDate = new Date( date );
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	    }
	    return age;
	}

	function rh_load_popup() {
		$.fancybox({
	        type 	: 'inline',
	        content : $('#user-edit').clone(),
	        afterClose : function() {
				$('.btn-action').removeAttr('data-action data-user data-cread').text( 'Delete' )
				.addClass('close-edit').attr('id', 'reading-update');
				$('.auto').attr('id', '').removeAttr('data-post data-user data-cread').addClass('close-edit').text( 'Cancel' );
				$('.edit-note').html('');
				$('.edit-subnote').html('');
				$('#manual-user').val('');
				$('.user-editor').removeClass('no-opt');
			},
			helpers : {
		        overlay : {
		            css : {
		                'background' : 'rgba(0, 0, 0, 0.47)'
		            }
		        }
		    }
	    });
	}

	function rh_select_temp (state) {
		if (!state.id) { return state.text; }
		var $state = $(
			'<span class="reader-el"><img src="'+ state.element.dataset.img +'" />' + state.text + '<i>'+state.element.dataset.average+' / '+state.element.dataset.total+'</i></span>'
		);
		return $state;
	}

	function loadvalue(input) {
        if (input.files && input.files[0]) {
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $('#rh-plch').attr('src', e.target.result );
                };
            });
        }
    }

})(jQuery);