;(function($) {
	'use strict';
	$.rhRequire = function( url, checker, callback, options ) {
		options = $.extend( options || {}, {
			dataType: "script",
			cache: true,
			url: url,
			success : callback
		});
		if ( typeof window[ checker ] === 'undefined' ) {
		    $.ajax( options );		
		    window[ checker ] = true;    
		}
	};
})(jQuery);