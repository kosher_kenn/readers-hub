;(function($) {
	'use strict';
	var ajax_loader = '<div class="ajax-loader"><span class="spinner">';

	$(window).load( function() {
		var current = sessionStorage.getItem('draft_'+$('#editing-id').val() )
		rh_autosave();
		$('.mce-flow-layout .mce-resizehandle').before('<span class="saved-stat"></span>');
		//$('#new_rh_reading').after('<div class="stat-holder"><span class="saved-stat"></span></div>');
		if ( current) {
			set_rh_text('.right-reading', current );
		}
	});
	$('body').on('click', '#new-reading', function(e) {
		var text = get_rh_text_content( '.content-index' );
		if ( !text ) return false;
		$('.site-content .wrap').prepend( ajax_loader );
		$(this).prop('disblaed', true);
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_readings',
				'security'	: rh_ajax.sec,
				'rh_req'	: rh_ajax.action,
				'rh_content': text,
			},
			success: function(data) { 
				$('.ajax-loader').remove();
				$('.page-content').html( data.status );
			},
			error: function(errorThrown) {
				$(this).prop('disblaed', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},	
		});
	});   

	$('body').on('click', '#leave-feedback', function(e) {
		var feedback = $('.rating-content').val();
		if ( !feedback ) return false;
		$('.site-content .wrap').prepend( ajax_loader );
		$(this).prop('disblaed', true);
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_readings',
				'security'	: rh_ajax.sec,
				'rh_req'	: 'insert_feedback',
				'rh_ID'		: $('#editing-id').val(),
				'feedback'	: feedback,
				'score'		: $('.rate-count').val(),
			},
			success: function(data) { 
				$('.ajax-loader').remove();
				if ( data.status !== false ) {
					$('.fed-res').html('<h3 class="rcaption" align="center">'+ data.status +'</h3>');					
				}
				console.log( data );
			},
			error: function(errorThrown) {
				$(this).prop('disblaed', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},	
		});
	}); 

	$('body').on('click', 'li.reading-item', function () {
		if ( $(this).hasClass('active') ) return;
		var id = $(this).data('id'),
			me = $(this),
			local = sessionStorage.getItem('draft_' + id );
		
		$('.right-reading').prepend( ajax_loader );
		if( local ) {
			$('.ajax-loader').remove()
			set_rh_text('.reading-content', local);
			$('.reading-item, .reading-msg').removeClass('active');
			me.addClass('active');
			$('#editing-id').val( id );
			$('#reading-'+ id ).addClass('active');
			return false;
		} 
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_readings',
				'security'	: rh_ajax.sec,
				'rh_req'	: 'load_comment',
				'rh_ID'		: id,
			},
			success: function(data) { 
				$('.ajax-loader').remove();
				$('.reading-item, .reading-msg').removeClass('active');
				me.addClass('active');
				$('#editing-id').val( id );
				$('#reading-'+ id ).addClass('active');
				set_rh_text('.reading-content', data.draft);
				console.log(data);
			},
			error: function(errorThrown) {
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},
		});
	});

	$('body').on('click', '.send-reading-response', function () {
		var text	= get_rh_text_content('.reading-content'),
			id 		= $('#editing-id').val(),
			con 	= $('.reading-content'),
			me 		= $(this);
		if ( !text ) return false;
	    if ( !con.hasClass('normal-user')) {
	    	$('.right-reading').prepend( ajax_loader );
	    } else {
	    	$('.reading-content').prepend( ajax_loader );
	    }
	    $(this).prop('disblaed', true);
	    $.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_readings',
				'security'	: rh_ajax.sec,
				'rh_req'	: 'insert_response',
				'rh_ID'		: id,
				'rh_content': text,
				'rh_type'	: me.data('id'),
			},
			success: function(data) { 
				set_rh_text('.reading-content', '');
				$(this).prop('disblaed', false);
				$('.ajax-loader').remove();
				if ( data.status !== false ) {
					sessionStorage.setItem('draft_'+id, '' );
					if( !con.hasClass('normal-user') ) {
						if ( $('.reading-item').length < 2 && !me.hasClass('follow') ) {
							$('.reading-content').html(
								'<h3 class="rcaption" align="center">No More Request!</h3>'
							);
						}
						if ( $('body').hasClass('page-in-progress') && me.hasClass('follow') ) {
							$('#reading-' + data.id ).find('ul').append( '<li class="responses ftr-user">'+ data.comment +'</li>' ); 
						} else {
							$('.reading-item[data-id="'+ data.id +'"]').remove();
							$('#reading-' + data.id ).remove();
							$('.reading-item:first-of-type, .reading-msg:first-of-type').addClass('active');
							$('#editing-id').val( $('.reading-item:first-of-type').data('id') );
							
						}						
						
					} else {
						$('.fed-res').html('<h3 class="rcaption">Response Successfully Sent!</h3>');
					}
					
				}
			},
			error: function(errorThrown) {
				$(this).prop('disblaed', false);
				$('.ajax-loader').remove();
				console.log( errorThrown );
			},	
		});
	});

	$('body').on('click', '.user-action .btn', function () {
		$('.user-action .btn').removeClass('active');
		$(this).addClass('active');
		if ( $(this).hasClass('folloup') ) {
			$('.normal-response').slideDown();
			$('.feedback-section').slideUp();
		} else {
			$('.normal-response').slideUp();
			$('.feedback-section').slideDown();
		}
	});

	$('.voting').bind('mousemove', function( e ) {
        var x = e.pageX - $(this).offset().left + 1;
        $(this).find('span').css({'width':x + 'px'});
        //console.log(x);
    });

    $('.voting').click( function() {
        var rate = $(this).children().width(),
        	rating = Math.ceil( parseFloat( rate ) / 22.4 ),
        	count = ( rating < .5 ) ? 1 : rating ;
        $('.rate-count').val( count );
        $('.rcount').width( parseFloat( count ) * 22.4 );
        if ( count == 1 ) {
        	$('.star-text').text('Poor');
        } else if ( count == 5 ) {
        	$('.star-text').text('Excellent');
        } else {
        	$('.star-text').text('');
        }
    });
    

	function get_rh_text_content( el ){
		if (typeof tinyMCE === 'undefined') return;
	    if ($(el).find('.wp-editor-wrap').hasClass('tmce-active')){
	        return tinyMCE.activeEditor.getContent();
	    } else{ 
	        return $(el).find('#new_rh_reading').val();
	    }
	}

	function set_rh_text( el, content ) {
		if (typeof tinyMCE === 'undefined') return;
		if ($(el).find('.wp-editor-wrap').hasClass('tmce-active')){
	        tinyMCE.activeEditor.setContent( content );
	    } else{ 
	        $(el).find('#new_rh_reading').val( content );
	    }
	}

	function rh_autosave() {
		if (typeof tinyMCE === 'undefined') return;
		if ( $('body').hasClass('page-readings') || $('body').hasClass('page-in-progress') ) {
			var active = ( tinyMCE.activeEditor ) ? 'visual' : 'html',
				interval = 2000,
				typingTimer,
				editor = ( tinyMCE.activeEditor ) ? tinyMCE.activeEditor : $('#new_rh_reading');
			editor.on('keyup', function(){
				clearTimeout(typingTimer);
				typingTimer = setTimeout(rh_after_type, interval);
				$('.saved-stat').text('Typing...');
			});
		}
	}

	function rh_after_type () {
		$('.saved-stat').text('Saved');
		sessionStorage.setItem('draft_'+$('#editing-id').val(), get_rh_text_content('.right-reading'));
		rh_draftsave();
	}

	function rh_draftsave() {
		var text = get_rh_text_content('.right-reading'),
			id 	 = $('#editing-id').val();
		$.ajax({
			type: 'POST',
			url: rh_ajax.url,
			data: {
				'action'	: 'rh_readings',
				'security'	: rh_ajax.sec,
				'rh_req'	: 'insert_draft',
				'rh_ID'		: id,
				'rh_content': text,
			},
			success: function(data) { 
				console.log( data );
			},
			error: function(errorThrown) {
				console.log( errorThrown );
			},	
		});
	}

})(jQuery);
